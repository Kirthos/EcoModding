// Copyright (c) Strange Loop Games. All rights reserved.
// See LICENSE file in the project root for full license information.
namespace Eco.Mods.TechTree
{
    using Eco.Core.Utils;
    using Eco.Gameplay.Auth;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Property;
    using Eco.Shared.Localization;
    using Eco.Shared.Math;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;

    [Serialized]
    public class StarterCampItem : WorldObjectItem<StarterCampObject>
    {
        public override LocString DisplayName { get { return Localizer.DoStr("Starter Camp"); } }
        public override LocString DisplayDescription { get { return Localizer.DoStr("A combination of a small tent and a tiny stockpile."); } }

        public override Result OnAreaValid(Player player, Vector3i position, Quaternion rotation)
        {
            Deed deed = PropertyManager.FindNearbyDeedOrCreate(player.User, position.XZ);

            foreach (var plot in WorldObject.GetOccupiedPropertyPositions(typeof(StarterCampObject), position, rotation))
                PropertyManager.Claim(deed.Id, player.User, player.User.Inventory, plot);

            var camp = WorldObjectManager.TryToAdd(typeof(CampsiteObject), player.User, position, rotation, false);
            var stockpile = WorldObjectManager.TryToAdd(typeof(TinyStockpileObject), player.User, position + rotation.RotateVector(Vector3i.Right * 3), rotation, false);
            player.User.OnWorldObjectPlaced.Invoke(camp);
            player.User.Markers.Add(camp.Position3i + Vector3i.Up, camp.UILinkContent());
            player.User.Markers.Add(stockpile.Position3i + Vector3i.Up, stockpile.UILinkContent());
            var storage = camp.GetComponent<PublicStorageComponent>();
            var changeSet = new InventoryChangeSet(storage.Inventory);
            PlayerDefaults.GetDefaultCampsiteInventory().ForEach(x =>
            {
                changeSet.AddItems(x.Key, x.Value, storage.Inventory);
            });
            changeSet.Apply();
            return Result.Succeeded;
        }

        public override bool ShouldCreate { get { return false; } }

        public override bool TryPlaceObject(Player player, Vector3i position, Quaternion rotation)
        {
            if (!TryPlaceObjectOnSolidGround(player, position, rotation))
                return false;

            foreach (var plot in WorldObject.GetOccupiedPropertyPositions(typeof(StarterCampObject), position, rotation))
            {
                var result = AuthManager.IsAuthorized(plot, player.User);
                if (!result.Success)
                {
                    player.SendTemporaryError(result.Message);
                    return false;
                }
            }

            return true;
        }
    }

    [Serialized]
    public class StarterCampObject : WorldObject
    {
        public override LocString DisplayName { get { return Localizer.DoStr("Starting Camp"); } }
    }
}