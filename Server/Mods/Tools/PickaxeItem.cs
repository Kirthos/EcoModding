﻿// Copyright (c) Strange Loop Games. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Eco.Mods.TechTree
{
    using System;
    using System.ComponentModel;
    using Core.Utils;
    using Eco.Core.Utils.AtomicAction;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Interactions;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Stats;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Shared.Items;
    using Eco.Shared.Localization;
    using Eco.World;
    using Eco.World.Blocks;
    using Gameplay.Objects;

    [Category("Hidden")]
    public partial class PickaxeItem : ToolItem
    {
        private static SkillModifiedValue caloriesBurn = CreateCalorieValue(20, typeof(MiningSkill), typeof(PickaxeItem), new PickaxeItem().UILink());
        static PickaxeItem() { }

        public override IDynamicValue CaloriesBurn            { get { return caloriesBurn; } }

        public override ClientPredictedBlockAction LeftAction { get { return ClientPredictedBlockAction.DestroyBlock; } }
        public override LocString LeftActionDescription          { get { return Localizer.DoStr("Mine"); } }

        private static IDynamicValue skilledRepairCost = new ConstantValue(1);
        public override IDynamicValue SkilledRepairCost { get { return skilledRepairCost; } }

        static IDynamicValue tier = new ConstantValue(0);
        public override IDynamicValue Tier { get { return tier; } }

        public override Item RepairItem { get { return Item.Get<StoneItem>(); } }
        public override int FullRepairAmount { get { return 1; } }

        public override InteractResult OnActLeft(InteractionContext context)
        {
            if (context.HasBlock && context.Block.Is<Minable>())
            {
                Item item = context.Block is IRepresentsItem ? Item.Get((IRepresentsItem)context.Block) : null;
                if (context.Block.Get<Minable>().Hardness <= this.Tier.GetCurrentValue(context.Player.User))
                {
                    Result result;
                    if (item != null)
                    {
                        IAtomicAction lawAction = PlayerActions.PickUp.CreateAtomicAction(context.Player.User, item, context.BlockPosition.Value);
                        result = this.PlayerDeleteBlock(context.BlockPosition.Value, context.Player, false, 1, null, lawAction);
                    }
                    else
                        result = this.PlayerDeleteBlock(context.BlockPosition.Value, context.Player, false, 1);

                    if (result.Success)
                    {
                        var forced = context.Player.User.Talentset.HasTalent(typeof(MiningLuckyBreakTalent)) ? 4 : -1;
                        if (RubbleObject.TrySpawnFromBlock(context.Block.GetType(), context.BlockPosition.Value, forced))
                        {
                            var user = context.Player.User;
                            var addition = item != null ? " " + (item.UILink()) : string.Empty;
                            this.AddExperience(user, 1.0f, new LocString(Localizer.Format("mining") + addition));
                            user.UserUI.OnCreateRubble.Invoke();
                        }
                    }

                    return (InteractResult)result;
                }
                else
                    context.Player.SendTemporaryError(Localizer.Format("Your pickaxe is too weak to break {0}.", item.UILink()));
            }
            else if (context.Target is RubbleObject)
            {
                var rubble = (RubbleObject)context.Target;
                if (rubble.IsBreakable)
                {
                    rubble.Breakup();
                    BurnCalories(context.Player);
                    return InteractResult.Success;
                }
                else
                    return InteractResult.NoOp;
            }

            if (context.Target is WorldObject) return BasicToolOnWorldObjectCheck(context);

            return InteractResult.NoOp;
        }

        public override bool ShouldHighlight(Type block)
        {
            return Block.Is<Minable>(block);
        }
    }
}
