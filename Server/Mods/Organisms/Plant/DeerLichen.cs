﻿namespace Eco.Mods.Organisms
{
    using Eco.Gameplay.Plants;
    using Eco.Mods.TechTree;
    using Eco.Shared.Localization;
    using Eco.Shared.Math;
    using Eco.Shared.Serialization;
    using Eco.Simulation;
    using Eco.Simulation.Types;
    using Eco.World.Blocks;

    [Serialized]
    public class DeerLichen : PlantEntity
    {
        public DeerLichen(WorldPosition3i mapPos, PlantPack plantPack) : base(species, mapPos, plantPack) { }
        public DeerLichen() { }
        static PlantSpecies species;
        public class DeerLichenSpecies : PlantSpecies
        {
            public DeerLichenSpecies() : base()
            {
                species = this;
                this.InstanceType = typeof(DeerLichen);

                // Info
                this.Decorative = false;
                this.Name = "DeerLichen";
                this.DisplayName = Localizer.DoStr("Deer Lichen");
                // Lifetime
                this.MaturityAgeDays = 0.7f;
                // Generation
                this.Water = false;
                // Food
                this.CalorieValue = 1f;
                // Resources
                this.SeedDropChance = 0.66f;
                this.SeedsAtGrowth = 0.6f;
                this.SeedsBonusAtGrowth = 0.9f;
                this.SeedRange = new Range(0f, 1f);
                this.SeedItemType = null;
                this.PostHarvestingGrowth = 0f;
                this.ScythingKills = true;
                this.PickableAtPercent = 0f;
                this.ResourceItemType = typeof(PlantFibersItem);
                this.ResourceRange = new Range(1f, 2f);
                this.ResourceBonusAtGrowth = 0.9f;
                // Visuals
                this.BlockType = typeof(DeerLichenBlock);
                // Climate
                this.ReleasesCO2ppmPerDay = -5E-06f;
                // WorldLayers
                this.MaxGrowthRate = 0.01f;
                this.MaxDeathRate = 0.005f;
                this.SpreadRate = 0.001f;
                this.ResourceConstraints.Add(new ResourceConstraint() { LayerName = "Phosphorus", HalfSpeedConcentration = 0.1f, MaxResourceContent = 0.2f });
                this.ResourceConstraints.Add(new ResourceConstraint() { LayerName = "SoilMoisture", HalfSpeedConcentration = 0.1f, MaxResourceContent = 0.05f });
                this.CapacityConstraints.Add(new CapacityConstraint() { CapacityLayerName = "FertileGround", ConsumedCapacityPerPop = 1f });
                this.CapacityConstraints.Add(new CapacityConstraint() { CapacityLayerName = "ShrubSpace", ConsumedCapacityPerPop = 3f });
                this.BlanketSpawnPercent = 0.6f;
                this.IdealTemperatureRange = new Range(0.11f, 0.23f);
                this.IdealMoistureRange = new Range(0.05f, 0.58f);
                this.TemperatureExtremes = new Range(0.1f, 0.31f);
                this.MoistureExtremes = new Range(0.0f, 0.62f);
                this.MaxPollutionDensity = 0.7f;
                this.PollutionDensityTolerance = 0.1f;
                this.VoxelsPerEntry = 5;

            }
        }
    }

    [Serialized, Reapable]
    public class DeerLichenBlock : PlantBlock
    {
        protected DeerLichenBlock() { }
        public DeerLichenBlock(WorldPosition3i position) : base(position) { }
    }
}