namespace Eco.Mods.Organisms
{
    using Eco.Gameplay.Plants;
    using Eco.Mods.TechTree;
    using Eco.Shared.Localization;
    using Eco.Shared.Math;
    using Eco.Shared.Serialization;
    using Eco.Simulation;
    using Eco.Simulation.Types;
    using Eco.World.Blocks;

    [Serialized]
    public class Buttonbush : PlantEntity
    {
        public Buttonbush(WorldPosition3i mapPos, PlantPack plantPack) : base(species, mapPos, plantPack) { }
        public Buttonbush() { }
        static PlantSpecies species;
        public class ButtonbushSpecies : PlantSpecies
        {
            public ButtonbushSpecies() : base()
            {
                species = this;
                this.InstanceType = typeof(Buttonbush);

                // Info
                this.Decorative = false;
                this.Name = "Buttonbush";
                this.DisplayName = Localizer.DoStr("Buttonbush");
                // Lifetime
                this.MaturityAgeDays = 1f;
                // Generation
                this.Water = false;
                // Food
                this.CalorieValue = 0.5f;
                // Resources
                this.SeedDropChance = 0.25f;
                this.SeedsAtGrowth = 0.6f;
                this.SeedsBonusAtGrowth = 0.9f;
                this.SeedRange = new Range(0f, 0f);
                this.SeedItemType = null;
                this.PostHarvestingGrowth = 0f;
                this.ScythingKills = true;
                this.PickableAtPercent = 0f;
                this.ResourceItemType = typeof(PlantFibersItem);
                this.ResourceRange = new Range(0f, 0f);
                this.ResourceBonusAtGrowth = 0.9f;
                this.RequireHarvestable = false;
                // Visuals
                this.BlockType = typeof(ButtonbushBlock);
                // Climate
                this.ReleasesCO2ppmPerDay = 0f;
                // WorldLayers
                this.MaxGrowthRate = 0.01f;
                this.MaxDeathRate = 0.005f;
                this.SpreadRate = 0.001f;
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Nitrogen", HalfSpeedConcentration = 0.1f, MaxResourceContent = 0.2f });
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Phosphorus", HalfSpeedConcentration = 0.1f, MaxResourceContent = 0.2f });
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Potassium", HalfSpeedConcentration = 0.1f, MaxResourceContent = 0.2f });
                this.CapacityConstraints.Add( new CapacityConstraint() { CapacityLayerName = "FertileGround" , ConsumedCapacityPerPop = 1f });
                this.CapacityConstraints.Add( new CapacityConstraint() { CapacityLayerName = "ShrubSpace" , ConsumedCapacityPerPop = 3f });
                this.IdealTemperatureRange = new Range(20f, 26f);
                this.IdealMoistureRange = new Range(0.75f, 0.9f);
                this.TemperatureExtremes = new Range(14f, 32f);
                this.MoistureExtremes = new Range(0.4f, 1.2f);
                this.MaxPollutionDensity = 0.7f;
                this.PollutionDensityTolerance = 0.1f;
                this.VoxelsPerEntry = 5;
            }
        }
    }

    [Serialized, Reapable, MoveEfficiency(0.333333333333333f)]
    public class ButtonbushBlock : PlantBlock
    {
        protected ButtonbushBlock() { }
        public ButtonbushBlock(WorldPosition3i position) : base(position) { }
    }
}