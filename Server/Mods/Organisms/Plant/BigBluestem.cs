﻿namespace Eco.Mods.Organisms
{
    using Eco.Gameplay.Plants;
    using Eco.Mods.TechTree;
    using Eco.Shared.Localization;
    using Eco.Shared.Math;
    using Eco.Shared.Serialization;
    using Eco.Simulation;
    using Eco.Simulation.Types;
    using Eco.World.Blocks;

    [Serialized]
    public class BigBluestem : PlantEntity
    {
        public BigBluestem(WorldPosition3i mapPos, PlantPack plantPack) : base(species, mapPos, plantPack) { }
        public BigBluestem() { }
        static PlantSpecies species;
        public class BigBluestemSpecies : PlantSpecies
        {
            public BigBluestemSpecies() : base()
            {
                species = this;
                this.InstanceType = typeof(BigBluestem);

                // Info
                this.Decorative = false;
                this.Name = "BigBluestem";
                this.DisplayName = Localizer.DoStr("Big Bluestem");
                // Lifetime
                this.MaturityAgeDays = 0.8f;
                // Generation
                this.Water = false;
                // Food
                this.CalorieValue = 4f;
                // Resources
                this.SeedDropChance = 0.5f;
                this.SeedsAtGrowth = 0.6f;
                this.SeedsBonusAtGrowth = 0.9f;
                this.SeedRange = new Range(1f, 2f);
                this.SeedItemType = null;
                this.PostHarvestingGrowth = 0.2f;
                this.ScythingKills = false;
                this.PickableAtPercent = 0f;
                this.ResourceItemType = typeof(PlantFibersItem);
                this.ResourceRange = new Range(3f, 10f);
                this.ResourceBonusAtGrowth = 0.9f;
                this.RequireHarvestable = false;
                // Visuals
                this.BlockType = typeof(BigBluestemBlock);
                // Climate
                this.ReleasesCO2ppmPerDay = -1E-05f;
                // WorldLayers
                this.MaxGrowthRate = 0.01f;
                this.MaxDeathRate = 0.005f;
                this.SpreadRate = 0.001f;
                this.ResourceConstraints.Add(new ResourceConstraint() { LayerName = "Nitrogen", HalfSpeedConcentration = 0.01f, MaxResourceContent = 0.01f });
                this.ResourceConstraints.Add(new ResourceConstraint() { LayerName = "Phosphorus", HalfSpeedConcentration = 0.01f, MaxResourceContent = 0.01f });
                this.ResourceConstraints.Add(new ResourceConstraint() { LayerName = "Potassium", HalfSpeedConcentration = 0.01f, MaxResourceContent = 0.01f });
                this.ResourceConstraints.Add(new ResourceConstraint() { LayerName = "SoilMoisture", HalfSpeedConcentration = 0.01f, MaxResourceContent = 0.02f });
                this.CapacityConstraints.Add(new CapacityConstraint() { CapacityLayerName = "FertileGround", ConsumedCapacityPerPop = 1f });
                this.BlanketSpawnPercent = 0.35f;
                this.IdealTemperatureRange = new Range(0.63f, 0.79f);
                this.IdealMoistureRange = new Range(0.32f, 0.48f);
                this.TemperatureExtremes = new Range(0.6f, 0.82f);
                this.MoistureExtremes = new Range(0.28f, 0.52f);
                this.MaxPollutionDensity = 0.7f;
                this.PollutionDensityTolerance = 0.1f;
                this.VoxelsPerEntry = 5;
            }
        }
    }

    [Serialized, Reapable, MoveEfficiency(0.8f)]
    public class BigBluestemBlock : PlantBlock
    {
        protected BigBluestemBlock() { }
        public BigBluestemBlock(WorldPosition3i position) : base(position) { }
    }
}