namespace Eco.Mods.Organisms
{
    using Eco.Gameplay.Plants;
    using Eco.Mods.TechTree;
    using Eco.Shared.Localization;
    using Eco.Shared.Math;
    using Eco.Shared.Serialization;
    using Eco.Simulation;
    using Eco.Simulation.Types;
    using Eco.World.Blocks;

    [Serialized]
    public class Kelp : PlantEntity
    {
        public Kelp(WorldPosition3i mapPos, PlantPack plantPack) : base(species, mapPos, plantPack) { }
        public Kelp() { }
        static PlantSpecies species;
        public class KelpSpecies : PlantSpecies
        {
            public KelpSpecies() : base()
            {
                species = this;
                this.InstanceType = typeof(Kelp);

                // Info
                this.Decorative = false;
                this.Name = "Kelp";
                this.DisplayName = Localizer.DoStr("Kelp");
                // Lifetime
                this.MaturityAgeDays = 1f;
                // Generation
                this.Water = true;
                // Food
                this.CalorieValue = 1f;
                // Resources
                this.SeedDropChance = 0.25f;
                this.SeedsAtGrowth = 0.6f;
                this.SeedsBonusAtGrowth = 0.9f;
                this.SeedRange = new Range(0f, 0f);
                this.SeedItemType = typeof(KelpSeedItem);
                this.PostHarvestingGrowth = 0f;
                this.ScythingKills = true;
                this.PickableAtPercent = 0f;
                this.ResourceItemType = typeof(KelpItem);
                this.ResourceRange = new Range(1f, 2f);
                this.ResourceBonusAtGrowth = 0.9f;
                // Visuals
                this.BlockType = typeof(KelpBlock);
                // Climate
                this.ReleasesCO2ppmPerDay = 0f;
                // WorldLayers
                this.MaxGrowthRate = 0.01f;
                this.MaxDeathRate = 0.005f;
                this.SpreadRate = 0.001f;
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Nitrogen", HalfSpeedConcentration = 0.1f, MaxResourceContent = 0.05f });
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Phosphorus", HalfSpeedConcentration = 0.1f, MaxResourceContent = 0.05f });
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Potassium", HalfSpeedConcentration = 0.1f, MaxResourceContent = 0.05f });
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "SoilMoisture", HalfSpeedConcentration = 0.1f, MaxResourceContent = 0.1f });
                this.CapacityConstraints.Add( new CapacityConstraint() { CapacityLayerName = "UnderwaterFertileGround" , ConsumedCapacityPerPop = 4f });
                this.BlanketSpawnPercent = 0.5f;
                this.IdealTemperatureRange = new Range(0.5f, 0.9f);
                this.IdealMoistureRange = new Range(0.1f, 0.9f);
                this.TemperatureExtremes = new Range(0.38f, 1f);
                this.MoistureExtremes = new Range(0f, 1f);
                this.MaxPollutionDensity = 0.7f;
                this.PollutionDensityTolerance = 0.1f;
                this.VoxelsPerEntry = 5;
            }
        }
    }

    [Serialized, UnderWater, Reapable, MoveEfficiency(0.5f)]
    public class KelpBlock : PlantBlock
    {
        protected KelpBlock() { }
        public KelpBlock(WorldPosition3i position) : base(position) { }
    }
}