namespace Eco.Mods.Organisms
{
    using Eco.Gameplay.Plants;
    using Eco.Mods.TechTree;
    using Eco.Shared.Localization;
    using Eco.Shared.Math;
    using Eco.Shared.Serialization;
    using Eco.Simulation;
    using Eco.Simulation.Types;
    using Eco.World.Blocks;

    [Serialized]
    public class Clam : PlantEntity
    {
        public Clam(WorldPosition3i mapPos, PlantPack plantPack) : base(species, mapPos, plantPack) { }
        public Clam() { }
        static PlantSpecies species;
        public class ClamSpecies : PlantSpecies
        {
            public ClamSpecies() : base()
            {
                species = this;
                this.InstanceType = typeof(Clam);

                // Info
                this.Decorative = false;
                this.Name = "Clam";
                this.DisplayName = Localizer.DoStr("Clam");
                // Lifetime
                this.MaturityAgeDays = 1f;
                // Generation
                this.Water = true;
                // Food
                this.CalorieValue = 5f;
                // Resources
                this.SeedDropChance = 0.25f;
                this.SeedsAtGrowth = 0.6f;
                this.SeedsBonusAtGrowth = 0.9f;
                this.SeedRange = new Range(0f, 0f);
                this.SeedItemType = null;
                this.PostHarvestingGrowth = 0f;
                this.ScythingKills = true;
                this.PickableAtPercent = 0f;
                this.ResourceItemType = typeof(ClamItem);
                this.ResourceRange = new Range(1f, 2f);
                this.ResourceBonusAtGrowth = 0.9f;
                // Visuals
                this.BlockType = typeof(ClamBlock);
                // Climate
                this.ReleasesCO2ppmPerDay = 0f;
                // WorldLayers
                this.MaxGrowthRate = 0.01f;
                this.MaxDeathRate = 0.005f;
                this.SpreadRate = 0.001f;
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Nitrogen", HalfSpeedConcentration = 0.01f, MaxResourceContent = 0.02f });
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Phosphorus", HalfSpeedConcentration = 0.01f, MaxResourceContent = 0.02f });
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Potassium", HalfSpeedConcentration = 0.01f, MaxResourceContent = 0.02f });
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "SoilMoisture", HalfSpeedConcentration = 0.01f, MaxResourceContent = 0.02f });
                this.CapacityConstraints.Add( new CapacityConstraint() { CapacityLayerName = "UnderwaterFertileGround" , ConsumedCapacityPerPop = 6f });
                this.BlanketSpawnPercent = 0.3f;
                this.IdealTemperatureRange = new Range(0.5f, 0.9f);
                this.IdealMoistureRange = new Range(0.1f, 0.9f);
                this.TemperatureExtremes = new Range(0.38f, 1f);
                this.MoistureExtremes = new Range(0f, 1f);
                this.MaxPollutionDensity = 0.7f;
                this.PollutionDensityTolerance = 0.1f;
                this.VoxelsPerEntry = 5;

            }
        }
    }

    [Serialized, UnderWater]
    public class ClamBlock : InteractablePlantBlock
    {
        protected ClamBlock() { }
        public ClamBlock(WorldPosition3i position) : base(position) { }
    }
}