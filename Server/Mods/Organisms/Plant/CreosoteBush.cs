namespace Eco.Mods.Organisms
{
    using Eco.Gameplay.Plants;
    using Eco.Mods.TechTree;
    using Eco.Shared.Localization;
    using Eco.Shared.Math;
    using Eco.Shared.Serialization;
    using Eco.Simulation;
    using Eco.Simulation.Types;
    using Eco.World.Blocks;

    [Serialized]
    public class CreosoteBush : PlantEntity
    {
        public CreosoteBush(WorldPosition3i mapPos, PlantPack plantPack) : base(species, mapPos, plantPack) { }
        public CreosoteBush() { }
        static PlantSpecies species;
        public class CreosoteBushSpecies : PlantSpecies
        {
            public CreosoteBushSpecies() : base()
            {
                species = this;
                this.InstanceType = typeof(CreosoteBush);

                // Info
                this.Decorative = false;
                this.Name = "CreosoteBush";
                this.DisplayName = Localizer.DoStr("Creosote Bush");
                // Lifetime
                this.MaturityAgeDays = 0.6f;
                // Generation
                this.Water = false;
                // Food
                this.CalorieValue = 2.5f;
                // Resources
                this.SeedDropChance = 0.5f;
                this.SeedsAtGrowth = 0.6f;
                this.SeedsBonusAtGrowth = 0.9f;
                this.SeedRange = new Range(1f, 2f);
                this.SeedItemType = typeof(CreosoteBushSeedItem);
                this.PostHarvestingGrowth = 0f;
                this.ScythingKills = true;
                this.PickableAtPercent = 0f;
                this.ResourceItemType = typeof(PlantFibersItem);
                this.ResourceRange = new Range(1f, 2f);
                this.ResourceBonusAtGrowth = 0.9f;
                this.RequireHarvestable = false;
                // Visuals
                this.BlockType = typeof(CreosoteBushBlock);
                // Climate
                this.ReleasesCO2ppmPerDay = -1E-05f;
                // WorldLayers
                this.MaxGrowthRate = 0.01f;
                this.MaxDeathRate = 0.005f;
                this.SpreadRate = 0.001f;
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Nitrogen", HalfSpeedConcentration = 0.4f, MaxResourceContent = 0.5f });
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Phosphorus", HalfSpeedConcentration = 0.1f, MaxResourceContent = 0.2f });
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Potassium", HalfSpeedConcentration = 0.2f, MaxResourceContent = 0.3f });
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "SoilMoisture", HalfSpeedConcentration = 0.3f, MaxResourceContent = 0.2f });
                this.CapacityConstraints.Add( new CapacityConstraint() { CapacityLayerName = "FertileGround" , ConsumedCapacityPerPop = 1f });
                this.CapacityConstraints.Add( new CapacityConstraint() { CapacityLayerName = "ShrubSpace" , ConsumedCapacityPerPop = 5f });
                this.GenerationSpawnCountPerPoint = new Range(5, 11);
                this.GenerationSpawnPointMultiplier = 0.05f;
                this.IdealTemperatureRange = new Range(0.75f, 0.85f);
                this.IdealMoistureRange = new Range(0.1f, 0.25f);
                this.TemperatureExtremes = new Range(0.7f, 1f);
                this.MoistureExtremes = new Range(0f, 0.35f);
                this.MaxPollutionDensity = 0.7f;
                this.PollutionDensityTolerance = 0.1f;
                this.VoxelsPerEntry = 5;
            }
        }
    }

    [Serialized, Reapable, MoveEfficiency(0.5f)]
    public class CreosoteBushBlock : PlantBlock
    {
        protected CreosoteBushBlock() { }
        public CreosoteBushBlock(WorldPosition3i position) : base(position) { }
    }
}