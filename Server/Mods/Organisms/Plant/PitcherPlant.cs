namespace Eco.Mods.Organisms
{
    using Eco.Gameplay.Plants;
    using Eco.Mods.TechTree;
    using Eco.Shared.Localization;
    using Eco.Shared.Math;
    using Eco.Shared.Serialization;
    using Eco.Simulation;
    using Eco.Simulation.Types;
    using Eco.World.Blocks;

    [Serialized]
    public class PitcherPlant : PlantEntity
    {
        public PitcherPlant(WorldPosition3i mapPos, PlantPack plantPack) : base(species, mapPos, plantPack) { }
        public PitcherPlant() { }
        static PlantSpecies species;
        public class PitcherPlantSpecies : PlantSpecies
        {
            public PitcherPlantSpecies() : base()
            {
                species = this;
                this.InstanceType = typeof(PitcherPlant);

                // Info
                this.Decorative = false;
                this.Name = "PitcherPlant";
                this.DisplayName = Localizer.DoStr("Pitcher Plant");
                // Lifetime
                this.MaturityAgeDays = 1f;
                // Generation
                this.Water = false;
                // Food
                this.CalorieValue = 1f;
                // Resources
                this.SeedDropChance = 0.25f;
                this.SeedsAtGrowth = 0.6f;
                this.SeedsBonusAtGrowth = 0.9f;
                this.SeedRange = new Range(0f, 0f);
                this.SeedItemType = null;
                this.PostHarvestingGrowth = 0f;
                this.ScythingKills = true;
                this.PickableAtPercent = 0f;
                this.ResourceItemType = typeof(PlantFibersItem);
                this.ResourceRange = new Range(0f, 0f);
                this.ResourceBonusAtGrowth = 0.9f;
                // Visuals
                this.BlockType = typeof(PitcherPlantBlock);
                // Climate
                this.ReleasesCO2ppmPerDay = 0f;
                // WorldLayers
                this.MaxGrowthRate = 0.01f;
                this.MaxDeathRate = 0.005f;
                this.SpreadRate = 0.001f;
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Nitrogen", HalfSpeedConcentration = 0.1f, MaxResourceContent = 0.1f });
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Phosphorus", HalfSpeedConcentration = 0.1f, MaxResourceContent = 0.1f });
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Potassium", HalfSpeedConcentration = 0.1f, MaxResourceContent = 0.1f });
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "SoilMoisture", HalfSpeedConcentration = 0.2f, MaxResourceContent = 0.2f });
                this.CapacityConstraints.Add( new CapacityConstraint() { CapacityLayerName = "FertileGround" , ConsumedCapacityPerPop = 1f });
                this.CapacityConstraints.Add( new CapacityConstraint() { CapacityLayerName = "ShrubSpace" , ConsumedCapacityPerPop = 3f });
                this.IdealTemperatureRange = new Range(21f, 24f);
                this.IdealMoistureRange = new Range(0.75f, 0.85f);
                this.TemperatureExtremes = new Range(14f, 30f);
                this.MoistureExtremes = new Range(0.55f, 1.1f);
                this.MaxPollutionDensity = 0.7f;
                this.PollutionDensityTolerance = 0.1f;
                this.VoxelsPerEntry = 5;

            }
        }
    }

    [Serialized, Reapable]
    public class PitcherPlantBlock : PlantBlock
    {
        protected PitcherPlantBlock() { }
        public PitcherPlantBlock(WorldPosition3i position) : base(position) { }
    }
}