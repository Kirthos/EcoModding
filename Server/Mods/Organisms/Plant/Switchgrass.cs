namespace Eco.Mods.Organisms
{
    using Eco.Gameplay.Plants;
    using Eco.Mods.TechTree;
    using Eco.Shared.Localization;
    using Eco.Shared.Math;
    using Eco.Shared.Serialization;
    using Eco.Simulation;
    using Eco.Simulation.Types;
    using Eco.World.Blocks;

    [Serialized]
    public class Switchgrass : PlantEntity
    {
        public Switchgrass(WorldPosition3i mapPos, PlantPack plantPack) : base(species, mapPos, plantPack) { }
        public Switchgrass() { }
        static PlantSpecies species;
        public class SwitchgrassSpecies : PlantSpecies
        {
            public SwitchgrassSpecies() : base()
            {
                species = this;
                this.InstanceType = typeof(Switchgrass);

                // Info
                this.Decorative = true;
                this.Name = "Switchgrass";
                this.DisplayName = Localizer.DoStr("Switchgrass");
                // Lifetime
                this.MaturityAgeDays = 0.5f;
                // Generation
                this.Water = false;
                // Food
                this.CalorieValue = 0.8f;
                // Resources
                this.SeedDropChance = 0f;
                this.SeedsAtGrowth = 0.6f;
                this.SeedsBonusAtGrowth = 0.9f;
                this.SeedRange = new Range(0f, 0f);
                this.SeedItemType = null;
                this.PostHarvestingGrowth = 0.2f;
                this.ScythingKills = false;
                this.PickableAtPercent = 0f;
                this.ResourceItemType = typeof(PlantFibersItem);
                this.ResourceRange = new Range(1f, 3f);
                this.ResourceBonusAtGrowth = 0.9f;
                this.RequireHarvestable = false;
                // Visuals
                this.BlockType = typeof(SwitchgrassBlock);
                // Climate
                this.ReleasesCO2ppmPerDay = -1E-06f;
                // WorldLayers
                this.MaxGrowthRate = 0.01f;
                this.MaxDeathRate = 0.005f;
                this.SpreadRate = 0.001f;
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Nitrogen", HalfSpeedConcentration = 0.01f, MaxResourceContent = 0.01f });
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Phosphorus", HalfSpeedConcentration = 0.01f, MaxResourceContent = 0.01f });
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Potassium", HalfSpeedConcentration = 0.01f, MaxResourceContent = 0.01f });
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "SoilMoisture", HalfSpeedConcentration = 0.01f, MaxResourceContent = 0.02f });
                this.CapacityConstraints.Add( new CapacityConstraint() { CapacityLayerName = "FertileGround" , ConsumedCapacityPerPop = 1f });
                this.BlanketSpawnPercent = 0.35f;
                this.IdealTemperatureRange = new Range(0.42f, 0.62f);
                this.IdealMoistureRange = new Range(0.32f, 0.48f);
                this.TemperatureExtremes = new Range(0.38f, 0.62f);
                this.MoistureExtremes = new Range(0.28f, 0.52f);
                this.MaxPollutionDensity = 1f;
                this.PollutionDensityTolerance = 0.1f;
                this.VoxelsPerEntry = 5;
            }
        }
    }

    [Serialized, Reapable, MoveEfficiency(0.8f)]
    public class SwitchgrassBlock : PlantBlock
    {
        protected SwitchgrassBlock() { }
        public SwitchgrassBlock(WorldPosition3i position) : base(position) { }
    }
}