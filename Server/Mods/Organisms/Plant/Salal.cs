namespace Eco.Mods.Organisms
{
    using Eco.Gameplay.Plants;
    using Eco.Mods.TechTree;
    using Eco.Shared.Localization;
    using Eco.Shared.Math;
    using Eco.Shared.Serialization;
    using Eco.Simulation;
    using Eco.Simulation.Types;
    using Eco.World.Blocks;

    [Serialized]
    public class Salal : PlantEntity
    {
        public Salal(WorldPosition3i mapPos, PlantPack plantPack) : base(species, mapPos, plantPack) { }
        public Salal() { }
        static PlantSpecies species;
        public class SalalSpecies : PlantSpecies
        {
            public SalalSpecies() : base()
            {
                species = this;
                this.InstanceType = typeof(Salal);

                // Info
                this.Decorative = false;
                this.Name = "Salal";
                this.DisplayName = Localizer.DoStr("Salal");
                // Lifetime
                this.MaturityAgeDays = 1f;
                // Generation
                this.Water = false;
                // Food
                this.CalorieValue = 0.5f;
                // Resources
                this.SeedDropChance = 0.25f;
                this.SeedsAtGrowth = 0.6f;
                this.SeedsBonusAtGrowth = 0.9f;
                this.SeedRange = new Range(0f, 0f);
                this.SeedItemType = null;
                this.PostHarvestingGrowth = 0f;
                this.ScythingKills = true;
                this.PickableAtPercent = 0f;
                this.ResourceItemType = typeof(PlantFibersItem);
                this.ResourceRange = new Range(1f, 3f);
                this.ResourceBonusAtGrowth = 0.9f;
                this.RequireHarvestable = false;
                // Visuals
                this.BlockType = typeof(SalalBlock);
                // Climate
                this.ReleasesCO2ppmPerDay = 0f;
                // WorldLayers
                this.MaxGrowthRate = 0.01f;
                this.MaxDeathRate = 0.005f;
                this.SpreadRate = 0.001f;
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Nitrogen", HalfSpeedConcentration = 0.08f, MaxResourceContent = 0.1f });
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Phosphorus", HalfSpeedConcentration = 0.05f, MaxResourceContent = 0.1f });
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Potassium", HalfSpeedConcentration = 0.05f, MaxResourceContent = 0.1f });
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "SoilMoisture", HalfSpeedConcentration = 0.05f, MaxResourceContent = 0.05f });
                this.CapacityConstraints.Add( new CapacityConstraint() { CapacityLayerName = "FertileGround" , ConsumedCapacityPerPop = 1f });
                this.BlanketSpawnPercent = 0.4f;
                this.IdealTemperatureRange = new Range(0.22f, 0.78f);
                this.IdealMoistureRange = new Range(0.52f, 0.58f);
                this.TemperatureExtremes = new Range(0.19f, 0.81f);
                this.MoistureExtremes = new Range(0.48f, 0.62f);
                this.MaxPollutionDensity = 0.7f;
                this.PollutionDensityTolerance = 0.1f;
                this.VoxelsPerEntry = 5;
            }
        }
    }

    [Serialized, Reapable, MoveEfficiency(0.5f)]
    public class SalalBlock : PlantBlock
    {
        protected SalalBlock() { }
        public SalalBlock(WorldPosition3i position) : base(position) { }
    }
}