namespace Eco.Mods.Organisms
{
    using Eco.Gameplay.Plants;
    using Eco.Mods.TechTree;
    using Eco.Shared.Localization;
    using Eco.Shared.Math;
    using Eco.Shared.Serialization;
    using Eco.Simulation;
    using Eco.Simulation.Types;
    using Eco.World.Blocks;

    [Serialized]
    public class Urchin : PlantEntity
    {
        public Urchin(WorldPosition3i mapPos, PlantPack plantPack) : base(species, mapPos, plantPack) { }
        public Urchin() { }
        static PlantSpecies species;
        public class UrchinSpecies : PlantSpecies
        {
            public UrchinSpecies() : base()
            {
                species = this;
                this.InstanceType = typeof(Urchin);

                // Info
                this.Decorative = false;
                this.Name = "Urchin";
                this.DisplayName = Localizer.DoStr("Urchin");
                // Lifetime
                this.MaturityAgeDays = 1f;
                // Generation
                this.Water = true;
                // Food
                this.CalorieValue = 5f;
                // Resources
                this.SeedDropChance = 0.25f;
                this.SeedsAtGrowth = 0.6f;
                this.SeedsBonusAtGrowth = 0.9f;
                this.SeedRange = new Range(0f, 0f);
                this.SeedItemType = null;
                this.PostHarvestingGrowth = 0f;
                this.ScythingKills = true;
                this.PickableAtPercent = 0f;
                this.ResourceItemType = typeof(UrchinItem);
                this.ResourceRange = new Range(1f, 2f);
                this.ResourceBonusAtGrowth = 0.9f;
                // Visuals
                this.BlockType = typeof(UrchinBlock);
                // Climate
                this.ReleasesCO2ppmPerDay = 0f;
                // WorldLayers
                this.MaxGrowthRate = 0.01f;
                this.MaxDeathRate = 0.005f;
                this.SpreadRate = 0.001f;
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Nitrogen", HalfSpeedConcentration = 0.1f, MaxResourceContent = 0.2f });
                this.CapacityConstraints.Add( new CapacityConstraint() { CapacityLayerName = "UnderwaterFertileGround" , ConsumedCapacityPerPop = 6f });
                this.BlanketSpawnPercent = 0.2f;
                this.IdealTemperatureRange = new Range(0.5f, 0.9f);
                this.IdealMoistureRange = new Range(0.1f, 0.9f);
                this.TemperatureExtremes = new Range(0.38f, 1f);
                this.MoistureExtremes = new Range(0f, 1f);
                this.MaxPollutionDensity = 0.7f;
                this.PollutionDensityTolerance = 0.1f;
                this.VoxelsPerEntry = 5;

            }
        }
    }

    [Serialized, UnderWater]
    public class UrchinBlock : InteractablePlantBlock
    {
        protected UrchinBlock() { }
        public UrchinBlock(WorldPosition3i position) : base(position) { }
    }
}