namespace Eco.Mods.Organisms
{
    using Eco.Gameplay.Plants;
    using Eco.Mods.TechTree;
    using Eco.Shared.Localization;
    using Eco.Shared.Math;
    using Eco.Shared.Serialization;
    using Eco.Simulation;
    using Eco.Simulation.Types;
    using Eco.World.Blocks;

    [Serialized]
    public class ArcticWillow : PlantEntity
    {
        public ArcticWillow(WorldPosition3i mapPos, PlantPack plantPack) : base(species, mapPos, plantPack) { }
        public ArcticWillow() { }
        static PlantSpecies species;
        public class ArcticWillowSpecies : PlantSpecies
        {
            public ArcticWillowSpecies() : base()
            {
                species = this;
                this.InstanceType = typeof(ArcticWillow);

                // Info
                this.Decorative = false;
                this.Name = "ArcticWillow";
                this.DisplayName = Localizer.DoStr("Arctic Willow");
                // Lifetime
                this.MaturityAgeDays = 0.6f;
                // Generation
                this.Water = false;
                // Food
                this.CalorieValue = 1f;
                // Resources
                this.SeedDropChance = 0.75f;
                this.SeedsAtGrowth = 0.6f;
                this.SeedsBonusAtGrowth = 0.9f;
                this.SeedRange = new Range(1f, 2f);
                this.SeedItemType = typeof(ArcticWillowSeedItem);
                this.PostHarvestingGrowth = 0f;
                this.ScythingKills = true;
                this.PickableAtPercent = 0f;
                this.ResourceItemType = typeof(PlantFibersItem);
                this.ResourceRange = new Range(1f, 3f);
                this.ResourceBonusAtGrowth = 0.9f;
                // Visuals
                this.BlockType = typeof(ArcticWillowBlock);
                // Climate
                this.ReleasesCO2ppmPerDay = -1E-05f;
                // WorldLayers
                this.MaxGrowthRate = 0.01f;
                this.MaxDeathRate = 0.005f;
                this.SpreadRate = 0.001f;
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Nitrogen", HalfSpeedConcentration = 0.1f, MaxResourceContent = 0.2f });
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Phosphorus", HalfSpeedConcentration = 0.15f, MaxResourceContent = 0.3f });
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "Potassium", HalfSpeedConcentration = 0.1f, MaxResourceContent = 0.2f });
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "SoilMoisture", HalfSpeedConcentration = 0.05f, MaxResourceContent = 0.1f });
                this.CapacityConstraints.Add( new CapacityConstraint() { CapacityLayerName = "FertileGround" , ConsumedCapacityPerPop = 1f });
                this.CapacityConstraints.Add( new CapacityConstraint() { CapacityLayerName = "ShrubSpace" , ConsumedCapacityPerPop = 2f });
                //this.GenerationSpawnSpread = new Range(1, 2);
                //this.GenerationSpawnCountPerPoint = new Range(4, 7);
                //this.GenerationSpawnPointMultiplier = 0.1f;
                this.BlanketSpawnPercent = 0.3f;
                this.IdealTemperatureRange = new Range(0.12f, 0.28f);
                this.IdealMoistureRange = new Range(0.36f, 0.48f);
                this.TemperatureExtremes = new Range(0.1f, 0.3f);
                this.MoistureExtremes = new Range(0.2f, 0.5f);
                this.MaxPollutionDensity = 0.7f;
                this.PollutionDensityTolerance = 0.1f;
                this.VoxelsPerEntry = 5;

            }
        }
    }
    [Serialized, Reapable]
    public class ArcticWillowBlock : PlantBlock
    {
        protected ArcticWillowBlock() { }
        public ArcticWillowBlock(WorldPosition3i position) : base(position) { }
    }
}