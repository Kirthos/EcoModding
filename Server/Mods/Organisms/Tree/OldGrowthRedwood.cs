namespace Eco.Mods.Organisms
{
    using System;
    using System.Collections.Generic;
    using Eco.Mods.TechTree;
    using Eco.Shared.Localization;
    using Eco.Shared.Math;
    using Eco.Shared.Serialization;
    using Eco.Simulation;
    using Eco.Simulation.Types;
    using Eco.World.Blocks;

    [Serialized]
    public class OldGrowthRedwood : TreeEntity
    {
        public OldGrowthRedwood(WorldPosition3i mapPos, PlantPack plantPack) : base(species, mapPos, plantPack) { }
        public OldGrowthRedwood() { }
        static TreeSpecies species;
        public class OldGrowthRedwoodSpecies : TreeSpecies
        {
            public OldGrowthRedwoodSpecies() : base()
            {
                species = this;
                this.InstanceType = typeof(OldGrowthRedwood);

                // Info
                this.Decorative = false;
                this.Name = "OldGrowthRedwood";
                this.DisplayName = Localizer.DoStr("Old Growth Redwood");
                // Lifetime
                this.TreeHealth = 200f;
                this.LogHealth = 2f;
                this.MaturityAgeDays = 1f;
                // Generation
                this.Water = false;
                // Food
                this.CalorieValue = 10f;
                // Resources
                this.ChanceToSpawnDebris = 0.4f;
                this.SeedDropChance = 0.25f;
                this.SeedsAtGrowth = 0.6f;
                this.SeedsBonusAtGrowth = 0.9f;
                this.SeedRange = new Range(0f, 0f);
                this.SeedItemType = null;
                this.PostHarvestingGrowth = 0f;
                this.ScythingKills = true;
                this.PickableAtPercent = 0f;
                this.ResourceItemType = typeof(LogItem);
                this.ResourceRange = new Range(450f, 500f);
                this.ResourceBonusAtGrowth = 0.9f;
                // Visuals
                this.BranchingDef = new List<TreeBranchDef>()
                {
                    new TreeBranchDef() { Name = "Branch0", Health = 3f, LeafPoints = 1, GrowthStartTime = new Range(0f, 0f), GrowthEndTime = new Range(1f, 1f) },
                    new TreeBranchDef() { Name = "Branch1", Health = 3f, LeafPoints = 1, GrowthStartTime = new Range(0f, 0f), GrowthEndTime = new Range(1f, 1f) },
                    new TreeBranchDef() { Name = "Branch2", Health = 3f, LeafPoints = 1, GrowthStartTime = new Range(0f, 0f), GrowthEndTime = new Range(1f, 1f) },
                    new TreeBranchDef() { Name = "Branch3", Health = 3f, LeafPoints = 1, GrowthStartTime = new Range(0f, 0f), GrowthEndTime = new Range(1f, 1f) },
                    new TreeBranchDef() { Name = "Branch4", Health = 3f, LeafPoints = 1, GrowthStartTime = new Range(0f, 0f), GrowthEndTime = new Range(1f, 1f) },
                    new TreeBranchDef() { Name = "Branch5", Health = 3f, LeafPoints = 1, GrowthStartTime = new Range(0f, 0f), GrowthEndTime = new Range(1f, 1f) },
                    new TreeBranchDef() { Name = "Branch6", Health = 3f, LeafPoints = 1, GrowthStartTime = new Range(0f, 0f), GrowthEndTime = new Range(1f, 1f) },
                    new TreeBranchDef() { Name = "Branch7", Health = 3f, LeafPoints = 1, GrowthStartTime = new Range(0f, 0f), GrowthEndTime = new Range(1f, 1f) },
                    new TreeBranchDef() { Name = "Branch8", Health = 3f, LeafPoints = 1, GrowthStartTime = new Range(0f, 0f), GrowthEndTime = new Range(1f, 1f) },
                    new TreeBranchDef() { Name = "Attach_Tip", Health = 3f, LeafPoints = 3, GrowthStartTime = new Range(0f, 0f), GrowthEndTime = new Range(1f, 1f) },
                };
                this.TopBranchLeafPoints = 3;
                this.TopBranchHealth = 3;
                this.BranchRotations = null;
                this.RandomYRotation = false;
                this.BranchCount = new Range(10f, 10f);
                this.BlockType = typeof(TreeBlock);
                // Climate
                this.ReleasesCO2ppmPerDay = -0.002f;
                // WorldLayers
                this.MaxGrowthRate = 0.01f;
                this.MaxDeathRate = 0.001f;
                this.SpreadRate = 0f;
                this.ResourceConstraints.Add(new ResourceConstraint() { LayerName = "SoilMoisture", HalfSpeedConcentration = 0.1f, MaxResourceContent = 0.7f });
                this.CapacityConstraints.Add(new CapacityConstraint() { CapacityLayerName = "FertileGround", ConsumedCapacityPerPop = 1f });
                this.CapacityConstraints.Add(new CapacityConstraint() { CapacityLayerName = "CanopySpace", ConsumedCapacityPerPop = 20f });
                this.BlanketSpawnPercent = 0.3f;
                this.IdealTemperatureRange = new Range(0.22f, 0.48f);
                this.IdealMoistureRange = new Range(0.42f, 0.48f);
                this.TemperatureExtremes = new Range(0.18f, 0.52f);
                this.MoistureExtremes = new Range(0.39f, 0.51f);
                this.MaxPollutionDensity = 0.6f;
                this.PollutionDensityTolerance = 0.1f;
                this.VoxelsPerEntry = 20;
                this.DebrisType = typeof(OldGrowthRedwoodTreeDebrisBlock);
                this.DebrisResources = new Dictionary<Type, Range>()
                {
                    { typeof(WoodPulpItem), new Range(4, 5) },
                    { typeof(RedwoodSeedItem), new Range(0, 1) },
                };
                this.XZScaleRange = new Range(.7f, 1.2f);
                this.YScaleRange = new Range(.7f, 1.2f);
                this.Density = 550f;
            }
        }

        public override void RandomizeAge()
        {
            // old growth redwoods do not grow
            this.GrowthPercent = 1f;
            this.YieldPercent = 1f;
        }

        [OnDeserialized]
        void OnDeserialized()
        {
            // migration - fix spawned ages of redwoods
            this.GrowthPercent = 1f;
            this.YieldPercent = 1f;
        }
    }
}