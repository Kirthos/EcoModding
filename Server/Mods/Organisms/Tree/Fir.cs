namespace Eco.Mods.Organisms
{
    using System;
    using System.Collections.Generic;
    using Eco.Mods.TechTree;
    using Eco.Shared.Localization;
    using Eco.Shared.Math;
    using Eco.Shared.Serialization;
    using Eco.Simulation;
    using Eco.Simulation.Types;
    using Eco.World.Blocks;

    [Serialized]
    public class Fir : TreeEntity
    {
        public Fir(WorldPosition3i mapPos, PlantPack plantPack) : base(species, mapPos, plantPack) { }
        public Fir() { }
        static TreeSpecies species;
        public class FirSpecies : TreeSpecies
        {
            public FirSpecies() : base()
            {
                species = this;
                this.InstanceType = typeof(Fir);

                // Info
                this.Decorative = false;
                this.Name = "Fir";
                this.DisplayName = Localizer.DoStr("Fir");
                // Lifetime
                this.TreeHealth = 10f;
                this.LogHealth = 2f;
                this.MaturityAgeDays = 4f;
                // Generation
                this.Water = false;
                // Food
                this.CalorieValue = 10f;
                // Resources
                this.ChanceToSpawnDebris = 0.3f;
                this.SeedDropChance = 0.3f;
                this.SeedsAtGrowth = 0.6f;
                this.SeedsBonusAtGrowth = 0.9f;
                this.SeedRange = new Range(1f, 1f);
                this.SeedItemType = typeof(FirSeedItem);
                this.PostHarvestingGrowth = 0f;
                this.ScythingKills = true;
                this.PickableAtPercent = 0f;
                this.ResourceItemType = typeof(LogItem);
                this.ResourceRange = new Range(0f, 50f);
                this.ResourceBonusAtGrowth = 0.9f;
                // Visuals
                this.BranchingDef = new List<TreeBranchDef>()
                {
                    new TreeBranchDef() { Name = "Branch0", Health = 3f, LeafPoints = 1, GrowthStartTime = new Range(0f, 0f), GrowthEndTime = new Range(1f, 1f) },
                    new TreeBranchDef() { Name = "Branch1", Health = 3f, LeafPoints = 1, GrowthStartTime = new Range(0f, 0f), GrowthEndTime = new Range(1f, 1f) },
                    new TreeBranchDef() { Name = "Branch2", Health = 3f, LeafPoints = 1, GrowthStartTime = new Range(0f, 0f), GrowthEndTime = new Range(1f, 1f) },
                };
                this.TopBranchLeafPoints = 0;
                this.TopBranchHealth = 3;
                this.BranchRotations = null;
                this.RandomYRotation = true;
                this.BranchCount = new Range(3f, 3f);
                this.BlockType = typeof(TreeBlock);
                // Climate
                this.ReleasesCO2ppmPerDay = -0.002f;
                // WorldLayers
                this.MaxGrowthRate = 0.01f;
                this.MaxDeathRate = 0.005f;
                this.SpreadRate = 0.001f;
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "SoilMoisture", HalfSpeedConcentration = 0.1f, MaxResourceContent = 0.5f });
                this.CapacityConstraints.Add( new CapacityConstraint() { CapacityLayerName = "FertileGround" , ConsumedCapacityPerPop = 1f });
                this.CapacityConstraints.Add( new CapacityConstraint() { CapacityLayerName = "CanopySpace" , ConsumedCapacityPerPop = 26f });
                this.BlanketSpawnPercent = 0.5f;
                this.IdealTemperatureRange = new Range(0.23f, 0.35f);
                this.IdealMoistureRange = new Range(0.45f, 0.55f);
                this.TemperatureExtremes = new Range(0.19f, 0.4f);
                this.MoistureExtremes = new Range(0.23f, 0.61f);
                this.MaxPollutionDensity = 0.7f;
                this.PollutionDensityTolerance = 0.1f;
                this.VoxelsPerEntry = 20;
                this.DebrisType = typeof(FirTreeDebrisBlock);
                this.DebrisResources = new Dictionary<Type, Range>()
                {
                    { typeof(WoodPulpItem), new Range(4, 5) },
                    { typeof(FirSeedItem), new Range(0, 1) },
                };
                this.XZScaleRange = new Range(.8f, 1.2f);
                this.YScaleRange = new Range(.8f, 1.4f);
                this.Density = 450f;
            }
        }
    }
}
