namespace Eco.Mods.Organisms
{
    using System;
    using System.Collections.Generic;
    using Eco.Mods.TechTree;
    using Eco.Shared.Localization;
    using Eco.Shared.Math;
    using Eco.Shared.Serialization;
    using Eco.Simulation;
    using Eco.Simulation.Types;
    using Eco.World.Blocks;

    [Serialized]
    public class Oak : TreeEntity
    {
        public Oak(WorldPosition3i mapPos, PlantPack plantPack) : base(species, mapPos, plantPack) { }
        public Oak() { }
        static TreeSpecies species;
        public class OakSpecies : TreeSpecies
        {
            public OakSpecies() : base()
            {
                species = this;
                this.InstanceType = typeof(Oak);

                // Info
                this.Decorative = false;
                this.Name = "Oak";
                this.DisplayName = Localizer.DoStr("Oak");
                // Lifetime
                this.TreeHealth = 20f;
                this.LogHealth = 2f;
                this.MaturityAgeDays = 6f;
                // Generation
                this.Water = false;
                // Food
                this.CalorieValue = 50f;
                // Resources
                this.ChanceToSpawnDebris = 0.4f;
                this.SeedDropChance = 0.3f;
                this.SeedsAtGrowth = 0.6f;
                this.SeedsBonusAtGrowth = 0.9f;
                this.SeedRange = new Range(1f, 1f);
                this.SeedItemType = typeof(AcornItem);
                this.PostHarvestingGrowth = 0f;
                this.ScythingKills = true;
                this.PickableAtPercent = 0f;
                this.ResourceItemType = typeof(LogItem);
                this.ResourceRange = new Range(0f, 75f);
                this.ResourceBonusAtGrowth = 0.9f;
                // Visuals
                this.BranchingDef = new List<TreeBranchDef>()
                {
                    new TreeBranchDef() { Name = "Branch0", Health = 3f, LeafPoints = 1, GrowthStartTime = new Range(0.1f, 0.15f), GrowthEndTime = new Range(0.8f, 1f) },
                    new TreeBranchDef() { Name = "Branch1", Health = 3f, LeafPoints = 1, GrowthStartTime = new Range(0.15f, 0.2f), GrowthEndTime = new Range(0.8f, 1f) },
                    new TreeBranchDef() { Name = "Branch2", Health = 3f, LeafPoints = 1, GrowthStartTime = new Range(0.2f, 0.25f), GrowthEndTime = new Range(0.8f, 1f) },
                    new TreeBranchDef() { Name = "Branch3", Health = 3f, LeafPoints = 1, GrowthStartTime = new Range(0.25f, 0.3f), GrowthEndTime = new Range(0.8f, 1f) },
                    new TreeBranchDef() { Name = "Branch4", Health = 3f, LeafPoints = 1, GrowthStartTime = new Range(0.3f, 0.35f), GrowthEndTime = new Range(0.8f, 1f) },
                    new TreeBranchDef() { Name = "Branch5", Health = 3f, LeafPoints = 1, GrowthStartTime = new Range(0.35f, 0.4f), GrowthEndTime = new Range(0.8f, 1f) },
                    new TreeBranchDef() { Name = "Branch6", Health = 3f, LeafPoints = 1, GrowthStartTime = new Range(0.4f, 0.45f), GrowthEndTime = new Range(0.8f, 1f) },
                    new TreeBranchDef() { Name = "Branch7", Health = 3f, LeafPoints = 1, GrowthStartTime = new Range(0.45f, 0.5f), GrowthEndTime = new Range(0.8f, 1f) },
                };
                this.TopBranchLeafPoints = 1;
                this.TopBranchHealth = 3;
                this.BranchRotations = new float[] {0f, 90f, 180f, 270f};
                this.RandomYRotation = false;
                this.BranchCount = new Range(2f, 4f);
                this.BlockType = typeof(TreeBlock);
                // Climate
                this.ReleasesCO2ppmPerDay = -0.0025f;
                // WorldLayers
                this.MaxGrowthRate = 0.01f;
                this.MaxDeathRate = 0.005f;
                this.SpreadRate = 0.0005f;
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "SoilMoisture", HalfSpeedConcentration = 0.1f, MaxResourceContent = 0.2f });
                this.CapacityConstraints.Add( new CapacityConstraint() { CapacityLayerName = "FertileGround" , ConsumedCapacityPerPop = 1f });
                this.CapacityConstraints.Add( new CapacityConstraint() { CapacityLayerName = "CanopySpace" , ConsumedCapacityPerPop = 45f });
                this.GenerationSpawnPointMultiplier = 0.02f;
                this.GenerationSpawnSpread = new Range(5, 8);
                this.GenerationSpawnCountPerPoint = new Range(1, 3);
                this.IdealTemperatureRange = new Range(0.45f, 0.76f);
                this.IdealMoistureRange = new Range(0.43f, 0.47f);
                this.TemperatureExtremes = new Range(0.4f, 0.8f);
                this.MoistureExtremes = new Range(0.4f, 0.5f);
                this.MaxPollutionDensity = 0.7f;
                this.PollutionDensityTolerance = 0.1f;
                this.VoxelsPerEntry = 20;
                this.DebrisType = typeof(OakTreeDebrisBlock);
                this.DebrisResources = new Dictionary<Type, Range>()
                {
                    { typeof(WoodPulpItem), new Range(4, 5) },
                    { typeof(AcornItem), new Range(0, 1) },
                };
                this.XZScaleRange = new Range(.6f, 1.6f);
                this.YScaleRange = new Range(.8f, 1.4f);
                this.Density = 740f;
            }
        }
    }
}
