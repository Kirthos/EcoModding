namespace Eco.Mods.Organisms
{
    using System;
    using System.Collections.Generic;
    using Eco.Mods.TechTree;
    using Eco.Shared.Localization;
    using Eco.Shared.Math;
    using Eco.Shared.Serialization;
    using Eco.Simulation;
    using Eco.Simulation.Types;
    using Eco.World.Blocks;

    [Serialized]
    public class SaguaroCactus : TreeEntity
    {
        public SaguaroCactus(WorldPosition3i mapPos, PlantPack plantPack) : base(species, mapPos, plantPack) { }
        public SaguaroCactus() { }
        static TreeSpecies species;
        public class SaguaroCactusSpecies : TreeSpecies
        {
            public SaguaroCactusSpecies() : base()
            {
                species = this;
                this.InstanceType = typeof(SaguaroCactus);

                // Info
                this.Decorative = true;
                this.Name = "SaguaroCactus";
                this.DisplayName = Localizer.DoStr("Saguaro Cactus");
                // Lifetime
                this.TreeHealth = 7f;
                this.LogHealth = 2f;
                this.MaturityAgeDays = 3f;
                // Generation
                this.Water = false;
                // Food
                this.CalorieValue = 4f;
                // Resources
                this.ChanceToSpawnDebris = 0.2f;
                this.SeedDropChance = 0.25f;
                this.SeedsAtGrowth = 0.6f;
                this.SeedsBonusAtGrowth = 0.9f;
                this.SeedRange = new Range(0f, 0f);
                this.SeedItemType = null;
                this.PostHarvestingGrowth = 0f;
                this.ScythingKills = true;
                this.PickableAtPercent = 0f;
                this.ResourceItemType = typeof(LogItem);
                this.ResourceRange = new Range(0f, 20f);
                this.ResourceBonusAtGrowth = 0.9f;
                // Visuals
                this.BranchingDef = new List<TreeBranchDef>()
                {
                    new TreeBranchDef() { Name = "Branch0", Health = 3f, LeafPoints = 0, GrowthStartTime = new Range(0f, 0.05f), GrowthEndTime = new Range(0.4f, 0.6f) },
                    new TreeBranchDef() { Name = "Branch1", Health = 3f, LeafPoints = 0, GrowthStartTime = new Range(0f, 0.05f), GrowthEndTime = new Range(0.4f, 0.6f) },
                    new TreeBranchDef() { Name = "Branch2", Health = 3f, LeafPoints = 0, GrowthStartTime = new Range(0f, 0.05f), GrowthEndTime = new Range(0.4f, 0.6f) },
                    new TreeBranchDef() { Name = "Branch3", Health = 3f, LeafPoints = 0, GrowthStartTime = new Range(0f, 0.05f), GrowthEndTime = new Range(0.4f, 0.6f) },
                };
                this.TopBranchLeafPoints = 0;
                this.TopBranchHealth = 0;
                this.BranchRotations = new float[] {0f, 90f, 180f, 270f};
                this.RandomYRotation = false;
                this.BranchCount = new Range(1f, 3f);
                this.BlockType = typeof(TreeBlock);
                // Climate
                this.ReleasesCO2ppmPerDay = -0.0015f;
                // WorldLayers
                this.MaxGrowthRate = 0.01f;
                this.MaxDeathRate = 0.005f;
                this.SpreadRate = 0.001f;
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "SoilMoisture", HalfSpeedConcentration = 0.1f, MaxResourceContent = 0.2f });
                this.CapacityConstraints.Add( new CapacityConstraint() { CapacityLayerName = "FertileGround" , ConsumedCapacityPerPop = 4f });
                this.CapacityConstraints.Add( new CapacityConstraint() { CapacityLayerName = "CanopySpace" , ConsumedCapacityPerPop = 19f });
                this.GenerationSpawnCountPerPoint = new Range(3, 7);
                this.GenerationSpawnPointMultiplier = 0.05f;
                this.GenerationSpawnSpread = new Range(3, 6);
                this.IdealTemperatureRange = new Range(0.8f, 0.9f);
                this.IdealMoistureRange = new Range(0.1f, 0.2f);
                this.TemperatureExtremes = new Range(0.7f, 1f);
                this.MoistureExtremes = new Range(0f, 0.38f);
                this.MaxPollutionDensity = 0.7f;
                this.PollutionDensityTolerance = 0.1f;
                this.VoxelsPerEntry = 20;
                this.DebrisType = typeof(SaguaroCactusDebrisBlock);
                this.DebrisResources = new Dictionary<Type, Range>()
                {
                    { typeof(WoodPulpItem), new Range(4, 5) },
                    { typeof(SaguaroSeedItem), new Range(0, 1) },
                };
                this.XZScaleRange = new Range(.8f, 1.4f);
                this.YScaleRange = new Range(.8f, 1.4f);
                this.Density = 250f;
            }
        }
    }
}
