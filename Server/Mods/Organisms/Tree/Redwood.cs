namespace Eco.Mods.Organisms
{
    using System;
    using System.Collections.Generic;
    using Eco.Mods.TechTree;
    using Eco.Shared.Localization;
    using Eco.Shared.Math;
    using Eco.Shared.Serialization;
    using Eco.Simulation;
    using Eco.Simulation.Types;
    using Eco.World.Blocks;

    [Serialized]
    public class Redwood : TreeEntity
    {
        public Redwood(WorldPosition3i mapPos, PlantPack plantPack) : base(species, mapPos, plantPack) { }
        public Redwood() { }
        static TreeSpecies species;
        public class RedwoodSpecies : TreeSpecies
        {
            public RedwoodSpecies() : base()
            {
                species = this;
                this.InstanceType = typeof(Redwood);

                // Info
                this.Decorative = false;
                this.Name = "Redwood";
                this.DisplayName = Localizer.DoStr("Redwood");
                // Lifetime
                this.TreeHealth = 10f;
                this.LogHealth = 2f;
                this.MaturityAgeDays = 5f;
                // Generation
                this.Water = false;
                // Food
                this.CalorieValue = 10f;
                // Resources
                this.ChanceToSpawnDebris = 0.3f;
                this.SeedDropChance = 0.25f;
                this.SeedsAtGrowth = 0.6f;
                this.SeedsBonusAtGrowth = 0.9f;
                this.SeedRange = new Range(0f, 0f);
                this.SeedItemType = null;
                this.PostHarvestingGrowth = 0f;
                this.ScythingKills = true;
                this.PickableAtPercent = 0f;
                this.ResourceItemType = typeof(LogItem);
                this.ResourceRange = new Range(0f, 60f);
                this.ResourceBonusAtGrowth = 0.9f;
                // Visuals
                this.BranchingDef = new List<TreeBranchDef>()
                {
                    new TreeBranchDef() { Name = "Branch0", Health = 3f, LeafPoints = 1, GrowthStartTime = new Range(0f, 0f), GrowthEndTime = new Range(1f, 1f) },
                    new TreeBranchDef() { Name = "Branch1", Health = 3f, LeafPoints = 1, GrowthStartTime = new Range(0f, 0f), GrowthEndTime = new Range(1f, 1f) },
                    new TreeBranchDef() { Name = "Branch2", Health = 3f, LeafPoints = 1, GrowthStartTime = new Range(0f, 0f), GrowthEndTime = new Range(1f, 1f) },
                };
                this.TopBranchLeafPoints = 0;
                this.TopBranchHealth = 3;
                this.BranchRotations = null;
                this.RandomYRotation = false;
                this.BranchCount = new Range(3f, 3f);
                this.BlockType = typeof(TreeBlock);
                // Climate
                this.ReleasesCO2ppmPerDay = -0.002f;
                // WorldLayers
                this.MaxGrowthRate = 0.01f;
                this.MaxDeathRate = 0.005f;
                this.SpreadRate = 0.001f;
                this.ResourceConstraints.Add( new ResourceConstraint() { LayerName = "SoilMoisture", HalfSpeedConcentration = 0.1f, MaxResourceContent = 0.7f });
                this.CapacityConstraints.Add( new CapacityConstraint() { CapacityLayerName = "FertileGround" , ConsumedCapacityPerPop = 1f });
                this.CapacityConstraints.Add( new CapacityConstraint() { CapacityLayerName = "CanopySpace" , ConsumedCapacityPerPop = 20f });
                this.BlanketSpawnPercent = 0.3f;
                this.IdealTemperatureRange = new Range(0.22f, 0.48f);
                this.IdealMoistureRange = new Range(0.42f, 0.48f);
                this.TemperatureExtremes = new Range(0.18f, 0.52f);
                this.MoistureExtremes = new Range(0.39f, 0.51f);
                this.MaxPollutionDensity = 0.6f;
                this.PollutionDensityTolerance = 0.1f;
                this.VoxelsPerEntry = 20;
                this.DebrisType = typeof(RedwoodTreeDebrisBlock);
                this.DebrisResources = new Dictionary<Type, Range>()
                {
                    { typeof(WoodPulpItem), new Range(4, 5) },
                    { typeof(RedwoodSeedItem), new Range(0, 1) },
                };
                this.XZScaleRange = new Range(.8f, 1.4f);
                this.YScaleRange = new Range(.8f, 1.6f);
                this.Density = 450f;
            }
        }
    }
}
