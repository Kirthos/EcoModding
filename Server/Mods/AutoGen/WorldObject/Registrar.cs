namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Gameplay.Blocks;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.Components.Auth;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Economy;
    using Eco.Gameplay.Housing;
    using Eco.Gameplay.Interactions;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Minimap;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Property;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Gameplay.Pipes.LiquidComponents;
    using Eco.Gameplay.Pipes.Gases;
    using Eco.Gameplay.Systems.Tooltip;
    using Eco.Shared;
    using Eco.Shared.Math;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.Shared.View;
    using Eco.Shared.Items;
    using Eco.Gameplay.Pipes;
    using Eco.World.Blocks;
    

    [Serialized]
    public partial class RegistrarItem :
        WorldObjectItem<RegistrarObject> 
    {
        public override LocString DisplayName { get { return Localizer.DoStr("Registrar"); } } 
        public override LocString DisplayDescription  { get { return Localizer.DoStr("Allows the setting management of titles."); } }

        static RegistrarItem()
        {
            
        }

        
    }


    [RequiresSkill(typeof(LumberSkill), 0)]      
    public partial class RegistrarRecipe : Recipe
    {
        public RegistrarRecipe()
        {
            this.Products = new CraftingElement[]
            {
                new CraftingElement<RegistrarItem>(),
            };

            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<BrickItem>(typeof(LumberSkill), 40, LumberSkill.MultiplicativeStrategy),
                new CraftingElement<LumberItem>(typeof(LumberSkill), 60, LumberSkill.MultiplicativeStrategy),
                new CraftingElement<GoldIngotItem>(typeof(LumberSkill), 30, LumberSkill.MultiplicativeStrategy),
                new CraftingElement<PaperItem>(typeof(LumberSkill), 60, LumberSkill.MultiplicativeStrategy),          
            };
            this.CraftMinutes = CreateCraftTimeValue(typeof(RegistrarRecipe), Item.Get<RegistrarItem>().UILink(), 60, typeof(LumberSkill), typeof(LumberFocusedSpeedTalent));    
            this.Initialize(Localizer.DoStr("Registrar"), typeof(RegistrarRecipe));
            CraftingComponent.AddRecipe(typeof(SawmillObject), this);
        }
    }
}