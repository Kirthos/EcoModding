namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Gameplay.Blocks;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.Components.Auth;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Economy;
    using Eco.Gameplay.Housing;
    using Eco.Gameplay.Interactions;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Minimap;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Property;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Gameplay.Pipes.LiquidComponents;
    using Eco.Gameplay.Pipes.Gases;
    using Eco.Gameplay.Systems.Tooltip;
    using Eco.Shared;
    using Eco.Shared.Math;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.Shared.View;
    using Eco.Shared.Items;
    using Eco.Gameplay.Pipes;
    using Eco.World.Blocks;
    
    [Serialized]
    [RequireComponent(typeof(LiquidProducerComponent))]         
    [RequireComponent(typeof(AttachmentComponent))]             
    [RequireComponent(typeof(PropertyAuthComponent))]
    [RequireComponent(typeof(MinimapComponent))]                
    [RequireComponent(typeof(PowerGridComponent))]              
    [RequireComponent(typeof(PowerConsumptionComponent))]                     
    [RequireComponent(typeof(PumpComponent))]                   
    public partial class WaterPumpObject : 
        WorldObject,    
        IRepresentsItem
    {
        public override LocString DisplayName { get { return Localizer.DoStr("Water Pump"); } } 

        public virtual Type RepresentedItemType { get { return typeof(WaterPumpItem); } } 



        protected override void Initialize()
        {

            this.GetComponent<MinimapComponent>().Initialize("Misc");                                 
            this.GetComponent<PowerConsumptionComponent>().Initialize(75);                      
            this.GetComponent<PowerGridComponent>().Initialize(5, new MechanicalPower());        

            this.GetComponent<LiquidProducerComponent>().Setup(typeof(WaterItem), (int)(1 * 1000f), this.NamedOccupancyOffset("WaterOut"));  
        }

        public override void Destroy()
        {
            base.Destroy();
        }
       
    }

    [Serialized]
    public partial class WaterPumpItem :
        WorldObjectItem<WaterPumpObject> 
    {
        public override LocString DisplayName { get { return Localizer.DoStr("Water Pump"); } } 
        public override LocString DisplayDescription  { get { return Localizer.DoStr("Pumps water from a source into a pipe network."); } }

        static WaterPumpItem()
        {
            
        }

        
        [Tooltip(7)] private LocString PowerConsumptionTooltip { get { return new LocString(string.Format(Localizer.DoStr("Consumes: {0}w"), Text.Info(75))); } }  
    }


    [RequiresSkill(typeof(MechanicsSkill), 0)]      
    public partial class WaterPumpRecipe : Recipe
    {
        public WaterPumpRecipe()
        {
            this.Products = new CraftingElement[]
            {
                new CraftingElement<WaterPumpItem>(),
            };

            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<IronIngotItem>(typeof(MechanicsSkill), 20, MechanicsSkill.MultiplicativeStrategy),
                new CraftingElement<IronPipeItem>(typeof(MechanicsSkill), 30, MechanicsSkill.MultiplicativeStrategy),          
            };
            this.CraftMinutes = CreateCraftTimeValue(typeof(WaterPumpRecipe), Item.Get<WaterPumpItem>().UILink(), 20, typeof(MechanicsSkill), typeof(MechanicsFocusedSpeedTalent));    
            this.Initialize(Localizer.DoStr("Water Pump"), typeof(WaterPumpRecipe));
            CraftingComponent.AddRecipe(typeof(AssemblyLineObject), this);
        }
    }
}