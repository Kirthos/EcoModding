namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Skills;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using Gameplay.Systems.TextLinks;
    using Eco.Shared.Localization;

    public class CleanTunaRecipe : Recipe
    {
        public CleanTunaRecipe()
        {
            this.Products = new CraftingElement[]
            {
               new CraftingElement<RawFishItem>(5f),  

            };
            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<TunaItem>(1)  
            };
            this.Initialize(Localizer.DoStr("Clean Tuna"), typeof(CleanTunaRecipe));
            this.CraftMinutes = new ConstantValue(2);
            this.CraftMinutes = new ConstantValue(2); 
            CraftingComponent.AddRecipe(typeof(FisheryObject), this);
        }
    }
}