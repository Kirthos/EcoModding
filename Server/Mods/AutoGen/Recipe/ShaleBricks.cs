namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Skills;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using Gameplay.Systems.TextLinks;
    using Eco.Shared.Localization;

    [RequiresSkill(typeof(BricklayingSkill), 0)] 
    public class ShaleBricksRecipe : Recipe
    {
        public ShaleBricksRecipe()
        {
            this.Products = new CraftingElement[]
            {
               new CraftingElement<BrickItem>(1f),  

            };
            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<ShaleItem>(typeof(BricklayingSkill), 16, BricklayingSkill.MultiplicativeStrategy),
                new CraftingElement<ClayItem>(typeof(BricklayingSkill), 2, BricklayingSkill.MultiplicativeStrategy),
                new CraftingElement<PitchItem>(typeof(BricklayingSkill), 8, BricklayingSkill.MultiplicativeStrategy), 
            };
            this.Initialize(Localizer.DoStr("Shale Bricks"), typeof(ShaleBricksRecipe));
            this.CraftMinutes = CreateCraftTimeValue(typeof(ShaleBricksRecipe), this.UILink(), 1, typeof(BricklayingSkill), typeof(BricklayingFocusedSpeedTalent));    
            CraftingComponent.AddRecipe(typeof(KilnObject), this);
        }
    }
}