namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Skills;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using Gameplay.Systems.TextLinks;
    using Eco.Shared.Localization;

    [RequiresSkill(typeof(CookingSkill), 0)] 
    public class ExoticSaladRecipe : Recipe
    {
        public ExoticSaladRecipe()
        {
            this.Products = new CraftingElement[]
            {
               new CraftingElement<BasicSaladItem>(1f),  

            };
            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<PricklyPearFruitItem>(typeof(CookingSkill), 10, CookingSkill.MultiplicativeStrategy),
                new CraftingElement<CriminiMushroomsItem>(typeof(CookingSkill), 10, CookingSkill.MultiplicativeStrategy),
                new CraftingElement<RiceItem>(typeof(CookingSkill), 20, CookingSkill.MultiplicativeStrategy), 
            };
            this.Initialize(Localizer.DoStr("Exotic Salad"), typeof(ExoticSaladRecipe));
            this.CraftMinutes = CreateCraftTimeValue(typeof(ExoticSaladRecipe), this.UILink(), 2, typeof(CookingSkill), typeof(CookingFocusedSpeedTalent));    
            CraftingComponent.AddRecipe(typeof(CastIronStoveObject), this);
        }
    }
}