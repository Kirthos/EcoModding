namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Skills;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using Gameplay.Systems.TextLinks;
    using Eco.Shared.Localization;

    [RequiresSkill(typeof(CookingSkill), 0)] 
    public class MixedSaladRecipe : Recipe
    {
        public MixedSaladRecipe()
        {
            this.Products = new CraftingElement[]
            {
               new CraftingElement<BasicSaladItem>(1f),  

            };
            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<FiddleheadsItem>(typeof(CookingSkill), 20, CookingSkill.MultiplicativeStrategy),
                new CraftingElement<TomatoItem>(typeof(CookingSkill), 15, CookingSkill.MultiplicativeStrategy),
                new CraftingElement<FireweedShootsItem>(typeof(CookingSkill), 15, CookingSkill.MultiplicativeStrategy), 
            };
            this.Initialize(Localizer.DoStr("Mixed Salad"), typeof(MixedSaladRecipe));
            this.CraftMinutes = CreateCraftTimeValue(typeof(MixedSaladRecipe), this.UILink(), 2, typeof(CookingSkill), typeof(CookingFocusedSpeedTalent));    
            CraftingComponent.AddRecipe(typeof(CastIronStoveObject), this);
        }
    }
}