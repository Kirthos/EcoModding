namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Skills;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using Gameplay.Systems.TextLinks;
    using Eco.Shared.Localization;

    public class ProcessBasaltRecipe : Recipe
    {
        public ProcessBasaltRecipe()
        {
            this.Products = new CraftingElement[]
            {
               new CraftingElement<StoneItem>(1f),  

            };
            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<BasaltItem>(1)  
            };
            this.Initialize(Localizer.DoStr("Process Basalt"), typeof(ProcessBasaltRecipe));
            this.CraftMinutes = new ConstantValue(0.02f);
            this.CraftMinutes = new ConstantValue(0.02f); 
            CraftingComponent.AddRecipe(typeof(WorkbenchObject), this);
        }
    }
}