namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Skills;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using Gameplay.Systems.TextLinks;
    using Eco.Shared.Localization;

    [RequiresSkill(typeof(CementSkill), 0)] 
    public class LimestoneConcreteRecipe : Recipe
    {
        public LimestoneConcreteRecipe()
        {
            this.Products = new CraftingElement[]
            {
               new CraftingElement<ConcreteItem>(1f),  

            };
            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<LimestoneItem>(typeof(CementSkill), 30, CementSkill.MultiplicativeStrategy),
                new CraftingElement<GraniteItem>(typeof(CementSkill), 20, CementSkill.MultiplicativeStrategy), 
            };
            this.Initialize(Localizer.DoStr("Limestone Concrete"), typeof(LimestoneConcreteRecipe));
            this.CraftMinutes = CreateCraftTimeValue(typeof(LimestoneConcreteRecipe), this.UILink(), 2, typeof(CementSkill), typeof(CementFocusedSpeedTalent));    
            CraftingComponent.AddRecipe(typeof(CementKilnObject), this);
        }
    }
}