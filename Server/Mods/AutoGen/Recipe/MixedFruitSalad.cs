namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Skills;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using Gameplay.Systems.TextLinks;
    using Eco.Shared.Localization;

    [RequiresSkill(typeof(CookingSkill), 0)] 
    public class MixedFruitSaladRecipe : Recipe
    {
        public MixedFruitSaladRecipe()
        {
            this.Products = new CraftingElement[]
            {
               new CraftingElement<FruitSaladItem>(1f),  

            };
            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<HuckleberriesItem>(typeof(CookingSkill), 40, CookingSkill.MultiplicativeStrategy),
                new CraftingElement<BeetItem>(typeof(CookingSkill), 20, CookingSkill.MultiplicativeStrategy), 
            };
            this.Initialize(Localizer.DoStr("Mixed Fruit Salad"), typeof(MixedFruitSaladRecipe));
            this.CraftMinutes = CreateCraftTimeValue(typeof(MixedFruitSaladRecipe), this.UILink(), 2, typeof(CookingSkill), typeof(CookingFocusedSpeedTalent));    
            CraftingComponent.AddRecipe(typeof(CastIronStoveObject), this);
        }
    }
}