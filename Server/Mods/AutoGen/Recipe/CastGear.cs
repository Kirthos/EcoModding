namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Skills;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using Gameplay.Systems.TextLinks;
    using Eco.Shared.Localization;

    [RequiresSkill(typeof(AdvancedSmeltingSkill), 0)] 
    public class CastGearRecipe : Recipe
    {
        public CastGearRecipe()
        {
            this.Products = new CraftingElement[]
            {
               new CraftingElement<GearItem>(2f),  

            };
            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<SteelItem>(typeof(AdvancedSmeltingSkill), 2, AdvancedSmeltingSkill.MultiplicativeStrategy), 
            };
            this.Initialize(Localizer.DoStr("Cast Gear"), typeof(CastGearRecipe));
            this.CraftMinutes = CreateCraftTimeValue(typeof(CastGearRecipe), this.UILink(), 1, typeof(AdvancedSmeltingSkill), typeof(AdvancedSmeltingFocusedSpeedTalent));    
            CraftingComponent.AddRecipe(typeof(ElectricMachinistTableObject), this);
        }
    }
}