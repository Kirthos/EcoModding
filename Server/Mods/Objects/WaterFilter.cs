﻿// Copyright (c) Strange Loop Games. All rights reserved.
// See LICENSE file in the project root for full license information.

namespace Eco.Mods.TechTree
{
    using Eco.Core.Controller;
    using Eco.Gameplay.Components.Auth;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Systems.Chat;
    using Eco.Shared.Serialization;
    using Eco.Gameplay.Components;
    using Eco.Shared.Localization;
    using Eco.Gameplay.Pipes.LiquidComponents;
    using Eco.Gameplay.Systems.Tooltip;
    using Eco.Shared.Utils;
    using Eco.Gameplay.Systems.TextLinks;
    using System.Linq;

    public partial class WasteFilterItem : WorldObjectItem<WasteFilterObject>
    {
        [Serialized] float processedWater { get; set; }
        public const float WaterPerCompostBlock = 10f;

        public override void OnPickup(WorldObject placed)                   { this.processedWater = placed.GetComponent<FilterComponent>().ProcessedWater; }
        public override void OnWorldObjectPlaced(WorldObject placedObject)  { placedObject.GetComponent<FilterComponent>().ProcessedWater = processedWater; }
    }

    [RequireComponent(typeof(PropertyAuthComponent))]
    [RequireComponent(typeof(OnOffComponent))]
    [RequireComponent(typeof(FilterComponent))]
    [RequireComponent(typeof(AttachmentComponent))]
    public partial class WasteFilterObject : WorldObject
    {
    }

    [Serialized]
    [RequireComponent(typeof(LiquidConverterComponent))]
    [RequireComponent(typeof(LinkComponent))]
    [RequireComponent(typeof(StatusComponent))]
    [RequireComponent(typeof(MustBeOwnedComponent))]
    public class FilterComponent : WorldObjectComponent
    {
        [Serialized] int blocksCreated;
        [Serialized] public float ProcessedWater { get; set; }
        StatusElement status;
        
        PeriodicUpdateRealTime updateThrottle = new PeriodicUpdateRealTime(5);

        public override void Initialize()
        {
            this.status   = this.Parent.GetComponent<StatusComponent>().CreateStatusElement();
            var converter = this.Parent.GetComponent<LiquidConverterComponent>();
            converter.Setup(typeof(SewageItem), typeof(WaterItem), this.Parent.NamedOccupancyOffset("InputPort"), this.Parent.NamedOccupancyOffset("OutputPort"), 100, 0f);
            converter.OnConvert += Converted;
        }

        string DisplayStatus { get { return Localizer.Format("{0} of compost block currently filtered.", Text.StyledPercent(ProcessedWater / WasteFilterItem.WaterPerCompostBlock)); } }

        void Converted(int rawamount)
        {
            var amount = rawamount / 1000f;
            ProcessedWater += amount;
            while (ProcessedWater > WasteFilterItem.WaterPerCompostBlock)
            {
                var invs = this.Parent.GetComponent<LinkComponent>().GetSortedLinkedInventories(this.Parent.OwnerUser);
                var all  = invs.AllInventories.ToList();
                if (!invs.TryAddItem<CompostItem>())
                {
                    this.Parent.GetComponent<OnOffComponent>().On = false;
                    ChatManager.ServerMessageToPlayer(Localizer.Format("{0} disabled, no room left for filtered waste.", this.Parent.UILink()), this.Parent.OwnerUser, false);
                    this.status.SetStatusMessage(false, Localizer.DoStr("No room for filtered waste."));
                    this.Changed("DisplayStatus");
                    this.Parent.UpdateEnabledAndOperating();
                    return;
                }
                else
                { 
                    ProcessedWater -= WasteFilterItem.WaterPerCompostBlock;
                    this.status.SetStatusMessage(true, DisplayStatus);
                }
            }

            if (updateThrottle.DoUpdate)
                this.status.SetStatusMessage(true, DisplayStatus);
        }
    }
}