﻿using Eco.Core.Utils;
using Eco.Gameplay.Economy;
using Eco.Gameplay.Objects;
using Eco.Gameplay.Players;
using Eco.Gameplay.Property;
using Eco.Gameplay.Systems.Chat;
using Eco.Gameplay.Utils;
using Eco.Shared.Math;
using Eco.Shared.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Kirthos.Mods.ServerMods
{
    public class Testcommand : IChatCommandHandler
    {
        [ChatCommand("")]
        public static void test(User user)
        {
            int totalSkillPoint = 0;
            user.Skillset.Skills.ForEach(x => {
                totalSkillPoint += x.PointsSpent.Sum();
                x.ForceSetLevel(0);
            });
            user.UseXP(-totalSkillPoint);
        }

        [ChatCommand("Make contract to a test user")]
        public static void C(User user)
        {
            var otherUser = TestUtils.OtherUser(user);
            WorldObjectUtil.SpawnAndClaim("StorageChestObject", otherUser, user.Position.XZi);
            EconomyManager.Contracts.Contracts.ForEach(x => x.Client = otherUser.Name);
            WorldObjectManager.All.ForEach(x => x.GetComponent<ContractBoardComponent>()?.ContractUpdated());

        }
    }
}
