﻿/*
 * Copyright (c) 2018 [Kirthos]
 * 
 * Created by Kirthos 10/13/2018
 */
namespace Kirthos.Mods.Phlo
{
    using Eco.Gameplay.Animals;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Interactions;
    using Eco.Gameplay.Items;
    using Eco.Mods.TechTree;
    using Eco.Shared.Items;
    using Eco.Shared.Math;
    using Eco.Shared.Serialization;
    using Eco.Simulation;
    using System;
    using System.ComponentModel;
    using System.Linq;
    using Eco.Gameplay.Skills;
    using Eco.Shared.Localization;

    [Serialized]
    [Weight(1000)]
    [Category("Tool")]
    public class BeehiveSmokerItem : ToolItem
    {
        public override string Description { get { return "Uses Wood Pulp to daze Bees or to fuel Beehives."; } }
        public override string FriendlyName { get { return "Beehive Smoke Tool"; } }

        // these 2 lines are for reparing cost that depend on a skill
        private static SkillModifiedValue skilledRepairCost = new SkillModifiedValue(15, SteelworkingSkill.MultiplicativeStrategy, typeof(SteelworkingSkill), Localizer.DoStr("repair cost"));
        public override IDynamicValue SkilledRepairCost { get { return skilledRepairCost; } }

        // these 2 line are for the repair item and amount
        public override Item RepairItem { get { return Item.Get<CoalItem>(); } }
        public override int FullRepairAmount { get { return 15; } }

        public override float DurabilityRate { get { return DurabilityMax / 2000f; } }

        public override ClientPredictedBlockAction LeftAction { get { return ClientPredictedBlockAction.None; } }
        public override string LeftActionDescription { get { return "action"; } }

        //public float pauseTime = 1f;
        //public float actualTime;

        public override InteractResult OnActLeft(InteractionContext context)
        {
            //            if (actualTime + pauseTime > TimeUtil.SecondsF)
            //                return InteractResult.NoOp;
            //            actualTime = TimeUtil.SecondsF;


            float CalorieUse = 10f;
            if (context.Player.User.Inventory.Clothing.Stacks.Any(x => x.Item is AdminMaskItem))
                CalorieUse /= 2f;

            if (context.Player.User.Inventory.TryRemoveItem<WoodPulpItem>(context.Player.User)
                && context.Player.User.Stomach.Calories >= CalorieUse
                && this.Durability >= 0)
            {
                context.Player.User.Stomach.UseCalories(CalorieUse);
                this.UseDurability(1, context.Player);
                foreach (AnimalEntity animal in EcoSim.AnimalSim.All.Where(x => (x as AnimalEntity).Species.Name == "QueenBee"))
                {
                    float distance = Vector3.Distance(animal.Position, context.Player.Position);
                    if (distance < 3)
                    {
                        animal.TryApplyDamage(context.Player, 50, context);
                    }
                }
                if (context.HasTarget && context.Target is BeehiveObject)
                {
                    FuelSupplyComponent storage = (context.Target as BeehiveObject).GetComponent<FuelSupplyComponent>();
                    storage.Inventory.TryAddItems<SmokeItem>(1);
                }
            }
            return InteractResult.Success;
        }

        public override InteractResult OnActRight(InteractionContext context)
        {
            return InteractResult.NoOp;
        }

        public override InteractResult OnActInteract(InteractionContext context)
        {
            return InteractResult.NoOp;
        }

        public override bool ShouldHighlight(Type block)
        {
            return false;
        }
    }

    [RequiresSkill(typeof(GlassworkingSkill), 1)]
    [RepairRequiresSkill(typeof(GlassworkingSkill), 1)]
    public partial class BeehiveSmokerRecipe : Recipe
    {
        public BeehiveSmokerRecipe()
        {
            this.Products = new CraftingElement[] { new CraftingElement<BeehiveSmokerItem>() };
            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<GlassItem>(8),
                new CraftingElement<IronIngotItem>(2),
            };
            this.CraftMinutes = new ConstantValue(5);
            this.Initialize("Beehive Smoke Tool", typeof(BeehiveSmokerRecipe));
            CraftingComponent.AddRecipe(typeof(KilnObject), this);
        }
    }
}
