﻿namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Skills;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using Gameplay.Systems.TextLinks;

    [RequiresSkill(typeof(BasicESmeltingSkill), 1)]
    public class JDSmeltingIronRecipe : Recipe
    {
        public JDSmeltingIronRecipe()
        {
            this.Products = new CraftingElement[]
            {
               new CraftingElement<IronIngotItem>(1),
            };
            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<IronOreItem>(typeof(BasicESmeltingEfficiencySkill), 5, BasicESmeltingEfficiencySkill.MultiplicativeStrategy),
            };
            this.Initialize("JDSmeltingIron", typeof(JDSmeltingIronRecipe));
            this.CraftMinutes = CreateCraftTimeValue(typeof(JDSmeltingIronRecipe), this.UILink(), 8.0f, typeof(BasicESmeltingSpeedSkill));
            CraftingComponent.AddRecipe(typeof(JDFurnaceObject), this);
        }
    }
}