﻿namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Skills;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using Gameplay.Systems.TextLinks;

    [RequiresSkill(typeof(BasicEWorkingSkill), 1)]
    public class JDBrickRecipe : Recipe
    {
        public JDBrickRecipe()
        {
            this.Products = new CraftingElement[]
            {
               new CraftingElement<BrickItem>(1),
            };
            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<StoneItem>(typeof(BasicEWorkingEfficiencySkill), 20, BasicEWorkingEfficiencySkill.MultiplicativeStrategy),
                new CraftingElement<PitchItem>(typeof(BasicEWorkingEfficiencySkill), 5, BasicEWorkingEfficiencySkill.MultiplicativeStrategy),
            };
            this.Initialize("JDBrick", typeof(JDBrickRecipe));
            this.CraftMinutes = CreateCraftTimeValue(typeof(JDBrickRecipe), this.UILink(), 8.0f, typeof(BasicEWorkingSpeedSkill));
            CraftingComponent.AddRecipe(typeof(JDKilnObject), this);
        }
    }
}