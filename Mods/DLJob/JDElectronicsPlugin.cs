﻿using Eco.Core.Plugins.Interfaces;
using Eco.Core.Utils;
using Eco.Gameplay.Players;
using Eco.Mods.TechTree;

public class JDElectricalPlugin : IModKitPlugin, IInitializablePlugin
{
    public JDElectricalPlugin() { }
    public override string ToString()
    {
        return "JD Electrical";
    }

    public string GetStatus()
    {
        return "Running...";
    }

    public void Initialize(TimedTask timer)
    {
        UserManager.OnUserLoggedIn.Add(u =>
        {
            if (!u.Skillset.HasSkill(typeof(JDElectricalSkill)))
                u.Skillset.LearnSkill(typeof(JDElectricalSkill));
        });
    }
};