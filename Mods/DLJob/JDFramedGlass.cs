﻿namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Skills;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using Gameplay.Systems.TextLinks;

    [RequiresSkill(typeof(BasicEWorkingSkill), 1)]
    public class JDFramedGlassRecipe : Recipe
    {
        public JDFramedGlassRecipe()
        {
            this.Products = new CraftingElement[]
            {
               new CraftingElement<FramedGlassItem>(1),
            };
            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<GlassItem>(typeof(BasicEWorkingEfficiencySkill), 10, BasicEWorkingEfficiencySkill.MultiplicativeStrategy),
                new CraftingElement<SteelItem>(typeof(BasicEWorkingEfficiencySkill), 6, BasicEWorkingEfficiencySkill.MultiplicativeStrategy),
            };
            this.Initialize("JDFramedGlass", typeof(JDFramedGlassRecipe));
            this.CraftMinutes = CreateCraftTimeValue(typeof(JDFramedGlassRecipe), this.UILink(), 8.0f, typeof(BasicEWorkingSpeedSkill));
            CraftingComponent.AddRecipe(typeof(JDKilnObject), this);
        }
    }
}