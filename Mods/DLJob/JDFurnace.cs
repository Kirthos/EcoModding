﻿namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Gameplay.Blocks;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.Components.Auth;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Economy;
    using Eco.Gameplay.Housing;
    using Eco.Gameplay.Interactions;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Minimap;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Property;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Gameplay.Pipes.LiquidComponents;
    using Eco.Gameplay.Pipes.Gases;
    using Eco.Gameplay.Systems.Tooltip;
    using Eco.Shared;
    using Eco.Shared.Math;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.Shared.View;
    using Eco.Shared.Items;
    using Eco.Gameplay.Pipes;
    using Eco.World.Blocks;

    [Serialized]
    [RequireComponent(typeof(PropertyAuthComponent))]
    [RequireComponent(typeof(MinimapComponent))]
    [RequireComponent(typeof(LinkComponent))]
    [RequireComponent(typeof(CraftingComponent))]
    [RequireComponent(typeof(PowerGridComponent))]
    [RequireComponent(typeof(PowerConsumptionComponent))]
    //[RequireComponent(typeof(SolidGroundComponent))]            
    //[RequireComponent(typeof(RoomRequirementsComponent))]
    //[RequireRoomContainment]
    //[RequireRoomVolume(25)]                              
    //[RequireRoomMaterialTier(3, 100)]        
    public class JDFurnaceObject :
        WorldObject
    {
        public override string FriendlyName { get { return "Electric Furnace"; } }


        protected override void Initialize()
        {
            this.GetComponent<MinimapComponent>().Initialize("Crafting");
            this.GetComponent<PowerConsumptionComponent>().Initialize(1600);
            this.GetComponent<PowerGridComponent>().Initialize(10, new ElectricPower());



        }

        public override void Destroy()
        {
            base.Destroy();
        }

    }

    [Serialized]
    public class JDFurnaceItem : WorldObjectItem<JDFurnaceObject>
    {
        public override string FriendlyName { get { return "Electric Furnace"; } }
        public override string Description { get { return "A Electric Furnace!"; } }

        static JDFurnaceItem()
        {

        }

    }


    [RequiresSkill(typeof(BasicESmeltingSkill), 3)]
    public class JDFurnaceRecipe : Recipe
    {
        public JDFurnaceRecipe()
        {
            this.Products = new CraftingElement[]
            {
                new CraftingElement<JDFurnaceItem>(),
            };

            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<BrickItem>(typeof(BasicESmeltingEfficiencySkill), 30, BasicESmeltingEfficiencySkill.MultiplicativeStrategy),
                new CraftingElement<IronIngotItem>(typeof(BasicESmeltingEfficiencySkill), 30, BasicESmeltingEfficiencySkill.MultiplicativeStrategy),
                new CraftingElement<SteelItem>(typeof(BasicESmeltingEfficiencySkill), 30, BasicESmeltingEfficiencySkill.MultiplicativeStrategy),
            };
            SkillModifiedValue value = new SkillModifiedValue(360, IndustrialEngineeringSpeedSkill.MultiplicativeStrategy, typeof(BasicESmeltingSpeedSkill), Localizer.Do("craft time"));
            SkillModifiedValueManager.AddBenefitForObject(typeof(JDFurnaceRecipe), Item.Get<JDFurnaceItem>().UILink(), value);
            SkillModifiedValueManager.AddSkillBenefit(Item.Get<JDFurnaceItem>().UILink(), value);
            this.CraftMinutes = value;
            this.Initialize("Electric Furnace", typeof(JDFurnaceRecipe));
            CraftingComponent.AddRecipe(typeof(AnvilObject), this);
        }
    }
}