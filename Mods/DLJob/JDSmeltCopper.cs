﻿namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Skills;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using Gameplay.Systems.TextLinks;

    [RequiresSkill(typeof(BasicESmeltingSkill), 1)]
    public class JDSmeltCopperRecipe : Recipe
    {
        public JDSmeltCopperRecipe()
        {
            this.Products = new CraftingElement[]
            {
               new CraftingElement<CopperIngotItem>(1),
            };
            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<CopperOreItem>(typeof(BasicESmeltingEfficiencySkill), 5, BasicESmeltingEfficiencySkill.MultiplicativeStrategy),
            };
            this.Initialize("JDSmeltCopper", typeof(JDSmeltCopperRecipe));
            this.CraftMinutes = CreateCraftTimeValue(typeof(JDSmeltCopperRecipe), this.UILink(), 8.0f, typeof(BasicESmeltingSpeedSkill));
            CraftingComponent.AddRecipe(typeof(JDFurnaceObject), this);
        }
    }
}