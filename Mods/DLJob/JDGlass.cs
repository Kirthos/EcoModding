﻿namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Skills;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using Gameplay.Systems.TextLinks;

    [RequiresSkill(typeof(BasicEWorkingSkill), 1)]
    public class JDGlassRecipe : Recipe
    {
        public JDGlassRecipe()
        {
            this.Products = new CraftingElement[]
            {
               new CraftingElement<GlassItem>(1),
            };
            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<SandItem>(typeof(BasicEWorkingEfficiencySkill), 4, BasicEWorkingEfficiencySkill.MultiplicativeStrategy),
            };
            this.Initialize("JDGlass", typeof(JDGlassRecipe));
            this.CraftMinutes = CreateCraftTimeValue(typeof(JDGlassRecipe), this.UILink(), 8.0f, typeof(BasicEWorkingSpeedSkill));
            CraftingComponent.AddRecipe(typeof(JDKilnObject), this);
        }
    }
}