﻿namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Gameplay.Blocks;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using Eco.Gameplay.Pipes;

    [RequiresSkill(typeof(BasicEWorkingSkill), 2)]
    public partial class JDBatteryRecipe : Recipe
    {
        public JDBatteryRecipe()
        {
            this.Products = new CraftingElement[]
            {
                new CraftingElement<JDBatteryItem>(),
            };
            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<CopperWiringItem>(typeof(BasicEWorkingEfficiencySkill), 2, BasicEWorkingEfficiencySkill.MultiplicativeStrategy),
                new CraftingElement<JDUnchargedBatteryItem>(typeof(BasicEWorkingEfficiencySkill), 1, BasicEWorkingEfficiencySkill.MultiplicativeStrategy),
            };
            this.CraftMinutes = CreateCraftTimeValue(typeof(JDBatteryRecipe), Item.Get<JDBatteryItem>().UILink(), 10, typeof(SteelworkingSpeedSkill));
            this.Initialize("Battery", typeof(JDBatteryRecipe));

            CraftingComponent.AddRecipe(typeof(JDBatteryChargerObject), this);
        }
    }


    [Serialized]
    [Weight(1000)]
    [Fuel(5000)]
    [Currency]
    public partial class JDBatteryItem :
    Item
    {
        public override string FriendlyName { get { return "Battery"; } }
        public override string Description { get { return "be careful or you will be shocked."; } }

    }

}