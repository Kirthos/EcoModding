﻿namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Skills;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using Gameplay.Systems.TextLinks;

    [RequiresSkill(typeof(BasicESmeltingSkill), 1)]
    public class JDSteelRivetsRecipe : Recipe
    {
        public JDSteelRivetsRecipe()
        {
            this.Products = new CraftingElement[]
            {
               new CraftingElement<RivetItem>(1),
            };
            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<SteelItem>(typeof(BasicESmeltingEfficiencySkill), 1, BasicESmeltingEfficiencySkill.MultiplicativeStrategy),
            };
            this.Initialize("JDSteelRivets", typeof(JDSteelRivetsRecipe));
            this.CraftMinutes = CreateCraftTimeValue(typeof(JDSteelRivetsRecipe), this.UILink(), 8.0f, typeof(BasicESmeltingSpeedSkill));
            CraftingComponent.AddRecipe(typeof(JDFurnaceObject), this);
        }
    }
}