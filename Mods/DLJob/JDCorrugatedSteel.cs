﻿namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Gameplay.Blocks;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using Eco.Gameplay.Pipes;

    [RequiresSkill(typeof(SteelworkingSkill), 2)]
    public partial class JDCorrugatedSteelRecipe : Recipe
    {
        public JDCorrugatedSteelRecipe()
        {
            this.Products = new CraftingElement[]
            {
                new CraftingElement<CorrugatedSteelItem>(),
            };
            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<SteelItem>(typeof(SteelworkingEfficiencySkill), 2, SteelworkingEfficiencySkill.MultiplicativeStrategy),
            };
            this.CraftMinutes = CreateCraftTimeValue(typeof(JDCorrugatedSteelRecipe), Item.Get<CorrugatedSteelItem>().UILink(), 1, typeof(SteelworkingSpeedSkill));
            this.Initialize("JDCorrugatedSteel", typeof(JDCorrugatedSteelRecipe));

            CraftingComponent.AddRecipe(typeof(JDEHammerObject), this);
        }
    }
}