﻿namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Skills;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using Gameplay.Systems.TextLinks;

    [RequiresSkill(typeof(BasicESmeltingSkill), 1)]
    public class JDRebarRecipe : Recipe
    {
        public JDRebarRecipe()
        {
            this.Products = new CraftingElement[]
            {
               new CraftingElement<RebarItem>(1),
            };
            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<SteelItem>(typeof(BasicESmeltingEfficiencySkill), 5, BasicESmeltingEfficiencySkill.MultiplicativeStrategy),
            };
            this.Initialize("JDRebar", typeof(JDRebarRecipe));
            this.CraftMinutes = CreateCraftTimeValue(typeof(JDRebarRecipe), this.UILink(), 8.0f, typeof(BasicESmeltingSpeedSkill));
            CraftingComponent.AddRecipe(typeof(JDFurnaceObject), this);
        }
    }
}