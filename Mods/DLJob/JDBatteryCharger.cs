﻿namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Gameplay.Blocks;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.Components.Auth;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Economy;
    using Eco.Gameplay.Housing;
    using Eco.Gameplay.Interactions;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Minimap;
    using Eco.Gameplay.Objects;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Property;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Gameplay.Pipes.LiquidComponents;
    using Eco.Gameplay.Pipes.Gases;
    using Eco.Gameplay.Systems.Tooltip;
    using Eco.Shared;
    using Eco.Shared.Math;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.Shared.View;
    using Eco.Shared.Items;
    using Eco.Gameplay.Pipes;
    using Eco.World.Blocks;

    [Serialized]
    [RequireComponent(typeof(PropertyAuthComponent))]
    [RequireComponent(typeof(MinimapComponent))]
    [RequireComponent(typeof(LinkComponent))]
    [RequireComponent(typeof(CraftingComponent))]
    [RequireComponent(typeof(PowerGridComponent))]
    [RequireComponent(typeof(PowerConsumptionComponent))]
    //[RequireComponent(typeof(SolidGroundComponent))]            
    //[RequireComponent(typeof(RoomRequirementsComponent))]
    //[RequireRoomContainment]
    //[RequireRoomVolume(25)]                              
    //[RequireRoomMaterialTier(3, 100)]        
    public partial class JDBatteryChargerObject :
        WorldObject
    {
        public override string FriendlyName { get { return "Battery Charger"; } }


        protected override void Initialize()
        {
            this.GetComponent<MinimapComponent>().Initialize("Crafting");
            this.GetComponent<PowerConsumptionComponent>().Initialize(1600);
            this.GetComponent<PowerGridComponent>().Initialize(10, new ElectricPower());



        }

        public override void Destroy()
        {
            base.Destroy();
        }

    }

    [Serialized]
    public partial class JDBatteryChargerItem : WorldObjectItem<JDBatteryChargerObject>
    {
        public override string FriendlyName { get { return "Battery Charger"; } }
        public override string Description { get { return "A Industrial Battery Charger!"; } }

        static JDBatteryChargerItem()
        {

        }

    }


    [RequiresSkill(typeof(BasicEWorkingSkill), 1)]
    public partial class JDBatteryChargerRecipe : Recipe
    {
        public JDBatteryChargerRecipe()
        {
            this.Products = new CraftingElement[]
            {
                new CraftingElement<JDBatteryChargerItem>(),
            };

            this.Ingredients = new CraftingElement[]
            {
                new CraftingElement<SteelItem>(typeof(BasicEWorkingEfficiencySkill), 25, BasicEWorkingEfficiencySkill.MultiplicativeStrategy),
                new CraftingElement<IronIngotItem>(typeof(BasicEWorkingEfficiencySkill), 20, BasicEWorkingEfficiencySkill.MultiplicativeStrategy),
            };
            SkillModifiedValue value = new SkillModifiedValue(360, IndustrialEngineeringSpeedSkill.MultiplicativeStrategy, typeof(BasicEWorkingSpeedSkill), Localizer.Do("craft time"));
            SkillModifiedValueManager.AddBenefitForObject(typeof(JDBatteryChargerRecipe), Item.Get<JDBatteryChargerItem>().UILink(), value);
            SkillModifiedValueManager.AddSkillBenefit(Item.Get<JDBatteryChargerItem>().UILink(), value);
            this.CraftMinutes = value;
            this.Initialize("Battery Charger", typeof(JDBatteryChargerRecipe));
            CraftingComponent.AddRecipe(typeof(MachineShopObject), this);
        }
    }
}