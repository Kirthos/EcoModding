﻿using System;
using Eco.Gameplay.Skills;
using Eco.Mods.TechTree;
using Eco.Shared.Localization;
using Eco.Shared.Serialization;

/*
 * Copyright (c) 2018 [Kirthos]
 * 
 * Created by Kirthos 02/17/2019
 */

namespace Kirthos.Mods
{
    [Serialized]
    public partial class LoggingPulpCleanerTalentGroup : TalentGroup
    {
        public override LocString DisplayName { get { return Localizer.DoStr("Pulp cleaner"); } }
        public override LocString DisplayDescription { get { return Localizer.DoStr("When break a wood pulp, break others wood pulp around."); } }

        public LoggingPulpCleanerTalentGroup()
        {
            Talents = new Type[]
            {
                typeof(LoggingPulpCleanerTalentGroup),
            };
            this.OwningSkill = typeof(LoggingSkill);
            this.Level = 7;
        }
    }

    public partial class PulpCleanerTalent : Talent
    {
        public override bool Base { get { return true; } }
    }

    [Serialized]
    public partial class LoggingPulpCleanerTalent : PulpCleanerTalent
    {
        public override bool Base { get { return false; } }
        public override Type TalentGroupType { get { return typeof(LoggingPulpCleanerTalentGroup); } }
        public LoggingPulpCleanerTalent()
        {
            this.Value = 1;
        }
    }

    [Serialized]
    public partial class LoggingOneHitTalentGroup : TalentGroup
    {
        public override LocString DisplayName { get { return Localizer.DoStr("Expert lumberer"); } }
        public override LocString DisplayDescription { get { return Localizer.DoStr("Cut the tree trunk, ready to harvest."); } }

        public LoggingOneHitTalentGroup()
        {
            Talents = new Type[]
            {
                typeof(LoggingOneHitTalent),
            };
            this.OwningSkill = typeof(LoggingSkill);
            this.Level = 7;
        }
    }

    public partial class OneHitTalent : Talent
    {
        public override bool Base { get { return true; } }
    }

    [Serialized]
    public partial class LoggingOneHitTalent : OneHitTalent
    {
        public override bool Base { get { return false; } }
        public override Type TalentGroupType { get { return typeof(LoggingOneHitTalentGroup); } }
        public LoggingOneHitTalent()
        {
            this.Value = 1;
        }
    }
}