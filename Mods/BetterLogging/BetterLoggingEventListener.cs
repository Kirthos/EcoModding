﻿using Asphalt.Api.Event;
using Asphalt.Api.Event.PlayerEvents;
using Asphalt.Util;
using Eco.Gameplay.Auth;
using Eco.Gameplay.Interactions;
using Eco.Gameplay.Items;
using Eco.Mods.TechTree;
using Eco.Shared.Items;
using Eco.Shared.Localization;
using Eco.Shared.Math;
using Eco.Simulation.Agents;
using Eco.World;
using Eco.World.Blocks;
using System;
using System.Linq;
using System.Reflection;
using System.Text;

/*
 * Copyright (c) 2018 [Kirthos]
 * 
 * Created by Kirthos 04/29/2018
 */

namespace Kirthos.Mods.BetterLogging
{
    public class BetterLoggingEventListener
    {
        public BetterLoggingEventListener() { }

        [EventHandler]
        public void OnPlayerInterract(PlayerInteractEvent evt)
        {
            evt.SetCancelled(ContextOnInterraction(evt.Context));
        }

        private bool ContextOnInterraction(InteractionContext context)
        {
            if (context.Method == InteractionMethod.Left)
            {
                if (context.SelectedItem is AxeItem)
                {
                    if (context.HasBlock && AuthManager.IsAuthorized(context))
                    {
                        var block = World.GetBlock(context.BlockPosition.Value);
                        if (block.Is<TreeDebris>() && context.Player.User.Talentset.HasTalent(typeof(LoggingPulpCleanerTalent)))
                        {
                            foreach (Vector3i pos in WorldUtils.GetTopBlockPositionAroundPoint<TreeDebrisBlock>(context.Player.User, context.BlockPosition.Value, 4))
                            {
                                if (AuthManager.IsAuthorized(pos.XZ, context.Player.User))
                                {
                                    if (context.Player.User.Inventory.TryAddItems<WoodPulpItem>(5))
                                    {
                                        StringBuilder message = new StringBuilder();
                                        message.AppendLocStr("You received");
                                        message.Append(" ");
                                        message.Append(Item.Get("WoodPulpItem").UILinkAndNumber(5));
                                        context.Player.SendTemporaryMessage(message.ToStringLoc());
                                        World.DeleteBlock(pos);
                                    }
                                }
                            }
                        }
                    }
                    if (context.HasTarget && context.Target is TreeEntity)
                    {
                        TreeEntity tree = context.Target as TreeEntity;
                        if (context.Parameters != null && context.Parameters.ContainsKey("slice"))
                        {
                            if (context.Player.User.Talentset.HasTalent(typeof(LoggingOneHitTalent)))
                            {
                                TreeBranch[] branches = typeof(Tree).GetField("branches", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(tree) as TreeBranch[];
                                if (!branches.Where(branch => branch != null).Select(branch => branch.Health).Where(health => health != 0).Any())
                                {
                                    for (int i = 1; i < 20; i++)
                                    {
                                        typeof(TreeEntity).GetMethod("TrySliceTrunk", BindingFlags.Instance | BindingFlags.NonPublic).Invoke(tree, new object[] { context.Player, 0.05f * (float)i });
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }
    }
}
