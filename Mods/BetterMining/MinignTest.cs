﻿namespace Eco.Mods.TechTree
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Skills;
    using Eco.Mods.TechTree;
    using Eco.Shared.Items;
    using Eco.Shared.Localization;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.Shared.View;
    public partial class MiningTestTalent : Talent
    {
        public override bool Base { get { return true; } }
    }

    [Serialized]
    public partial class MiningMiningTestTalentGroup : TalentGroup
    {
        public override LocString DisplayName { get { return Localizer.DoStr("Sweeping Hands: Mining 1"); } }
        public override LocString DisplayDescription { get { return Localizer.DoStr("Picking up rocks also attempts to pick up similar rocks in an area."); } }

        public MiningMiningTestTalentGroup()
        {
            Talents = new Type[]
            {

            typeof(MiningMiningTestTalent),


            };
            this.OwningSkill = typeof(MiningSkill);
            this.Level = 7;
        }
    }


    [Serialized]
    public partial class MiningMiningTestTalent : MiningTestTalent
    {
        public override bool Base { get { return false; } }
        public override Type TalentGroupType { get { return typeof(MiningMiningTestTalentGroup); } }
        public MiningMiningTestTalent()
        {
            this.Value = 1;
        }
    }

    public partial class MiningTestTalent2 : Talent
    {
        public override bool Base { get { return true; } }
    }

    [Serialized]
    public partial class MiningMiningTestTalentGroup2 : TalentGroup
    {
        public override LocString DisplayName { get { return Localizer.DoStr("Sweeping Hands: Mining 2"); } }
        public override LocString DisplayDescription { get { return Localizer.DoStr("Picking up rocks also attempts to pick up similar rocks in an area."); } }

        public MiningMiningTestTalentGroup2()
        {
            Talents = new Type[]
            {

            typeof(MiningMiningTestTalent2),


            };
            this.OwningSkill = typeof(MiningSkill);
            this.Level = 7;
        }
    }


    [Serialized]
    public partial class MiningMiningTestTalent2 : MiningTestTalent2
    {
        public override bool Base { get { return false; } }
        public override Type TalentGroupType { get { return typeof(MiningMiningTestTalentGroup2); } }
        public MiningMiningTestTalent2()
        {
            this.Value = 1;
        }
    }

    public partial class MiningTestTalent3 : Talent
    {
        public override bool Base { get { return true; } }
    }

    [Serialized]
    public partial class MiningMiningTestTalentGroup3 : TalentGroup
    {
        public override LocString DisplayName { get { return Localizer.DoStr("Sweeping Hands: Mining 3"); } }
        public override LocString DisplayDescription { get { return Localizer.DoStr("Picking up rocks also attempts to pick up similar rocks in an area."); } }

        public MiningMiningTestTalentGroup3()
        {
            Talents = new Type[]
            {

            typeof(MiningMiningTestTalent3),


            };
            this.OwningSkill = typeof(MiningSkill);
            this.Level = 7;
        }
    }


    [Serialized]
    public partial class MiningMiningTestTalent3 : MiningTestTalent3
    {
        public override bool Base { get { return false; } }
        public override Type TalentGroupType { get { return typeof(MiningMiningTestTalentGroup3); } }
        public MiningMiningTestTalent3()
        {
            this.Value = 1;
        }
    }
}