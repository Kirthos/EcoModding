﻿using Eco.Core.Utils.AtomicAction;
using Eco.Gameplay.Interactions;
using Eco.Gameplay.Items;
using Eco.Gameplay.Objects;
using Eco.Gameplay.Players;
using Eco.Gameplay.Property;
using Eco.Gameplay.Utils;
using Eco.Mods.TechTree;
using Eco.Shared.Items;
using Eco.Shared.Math;
using Eco.Shared.Networking;
using Eco.Shared.Serialization;
using Eco.Shared.Utils;
using Eco.World;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kirthos.Mods.Testing
{
    class TestItem
    {
        public static void TestGiveAllItem(User user)
        {
            Stopwatch stopWatch = new Stopwatch();
            user.Inventory.Toolbar.Clear();
            user.Inventory.Carried.Clear();
            stopWatch.Restart();
            foreach (Item item in Item.AllItems)
            {
                Type t = item.GetType();
                try
                {
                    if (t.HasAttribute(typeof(SerializedAttribute)))
                    {
                        Console.WriteLine("Give Item " + t.ToString().Split('.').Last());
                        user.Inventory.TryAddItem(t);
                        // If it require shovel we don't try to force select a slot or the game crash
                        if ((t == typeof(DirtItem) || t == typeof(SandItem) || t == typeof(TailingsItem)) == false)
                            typeof(ToolbarInventory).GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Where(x => x.Name.Contains("SelectIndex")).First().Invoke(user.Inventory.Toolbar, new object[] { user.Player, 0 });
                        user.Inventory.Carried.Clear();
                        user.Inventory.Toolbar.Clear();
                        if (user.Client.Connected == false)
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine("User no more connected - Stopping actual test.");
                            Console.ResetColor();
                            return;
                        }
                    }
                }
                catch (Exception e)
                {
                    Testcommand.exceptions.Add(e);
                    Testcommand.exceptionMessage.Add("Exception throw while trying to give item " + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Exception throw while trying to give item " + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                    Console.ResetColor();
                }
            }
            stopWatch.Stop();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("All item given in " + stopWatch.ElapsedMilliseconds + "ms\n");
            Console.ResetColor();
        }

        public static void TestInteractAir(User user)
        {
            Stopwatch stopWatch = new Stopwatch();
            user.Inventory.Carried.Clear();
            user.Inventory.Toolbar.Clear();
            stopWatch.Restart();
            foreach (Item item in Item.AllItems)
            {
                Type t = item.GetType();
                try
                {
                    if (t.HasAttribute(typeof(SerializedAttribute)))
                    {
                        Console.Write("Using item " + t.ToString().Split('.').Last());
                        Item current = Item.Create(t);
                        user.Inventory.TryAddItem(current);
                        // If it require shovel we don't try to force select a slot or the game crash
                        if ((t == typeof(DirtItem) || t == typeof(SandItem) || t == typeof(TailingsItem)) == false)
                            typeof(ToolbarInventory).GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Where(x => x.Name.Contains("SelectIndex")).First().Invoke(user.Inventory.Toolbar, new object[] { user.Player, 0});
                        InteractionInfo info = new InteractionInfo();
                        InteractionContext context = null;
                        info.Method = InteractionMethod.Left;
                        context = info.MakeContext(user.Player);
                        Console.Write(" - Left");
                        current.OnActLeft(context);
                        info.Method = InteractionMethod.Right;
                        context = info.MakeContext(user.Player);
                        Console.Write(" - Right");
                        current.OnActRight(context);
                        info.Method = InteractionMethod.Interact;
                        context = info.MakeContext(user.Player);
                        Console.Write(" - interact");
                        current.OnActInteract(context);
                        Thread.Sleep(10);
                        Console.WriteLine();
                        user.Inventory.Carried.Clear();
                        user.Inventory.Toolbar.Clear();
                        if (user.Client.Connected == false)
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine("User no more connected - Stopping actual test.");
                            Console.ResetColor();
                            return;
                        }
                    }
                }
                catch (Exception e)
                {
                    Testcommand.exceptions.Add(e);
                    Console.WriteLine();
                    Testcommand.exceptionMessage.Add("Exception throw while using item " + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Exception throw while using item " + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                    Console.ResetColor();
                }
            }
            stopWatch.Stop();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("All item given in " + stopWatch.ElapsedMilliseconds + "ms\n");
            Console.ResetColor();
        }

        public static void TestPlaceWorldObjectItem(User user)
        {
            Stopwatch stopWatch = new Stopwatch();
            Vector3i userPos = user.Position.Ceiling;
            user.Inventory.Carried.Clear();
            user.Inventory.Toolbar.Clear();
            stopWatch.Restart();
            foreach (Item item in Item.AllItems)
            {
                Type t = item.GetType();
                try
                {
                    Block b = World.SetBlock(typeof(FlatSteelFloorBlock), userPos + Vector3i.Forward * 2);
                    for (int i = -2; i <= 2; i++)
                    {
                        for (int j = -2; j <= 2; j++)
                        {
                            if (i == 0 && j == 0) continue;
                            World.SetBlock(typeof(FlatSteelFloorBlock), new Vector3i(i, 0, j) + userPos + Vector3i.Forward * 2);
                        }
                    }
                    if (item is WorldObjectItem == false) continue;
                    if ((item as WorldObjectItem).WorldObjectType.BaseType == typeof(PhysicsWorldObject)) continue;
                    if (t.HasAttribute(typeof(SerializedAttribute)))
                    {
                        Console.Write("Place object with item " + t.ToString().Split('.').Last());
                        Item current = Item.Create(t);
                        user.Inventory.TryAddItem(current);
                        user.Player.SetPosition(userPos + Vector3i.Up * 1);
                        typeof(ToolbarInventory).GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Where(x => x.Name.Contains("SelectIndex")).First().Invoke(user.Inventory.Toolbar, new object[] { user.Player, 0 });
                        InteractionInfo info = new InteractionInfo();
                        InteractionContext context = null;
                        info.BlockPosition = userPos + Vector3i.Forward * 2 + Vector3i.Up * 10;
                        info.Distance = Vector3.Distance(user.Position, info.BlockPosition.Value);
                        info.Normal = Vector3i.Up;
                        info.Position = info.BlockPosition;
                        info.Method = InteractionMethod.Right;
                        context = info.MakeContext(user.Player);
                        user.Inventory.Toolbar.SelectedItem.OnActRight(context);
                        context.Player.RPC("AttemptPlaceObject", context.Player.Client, (Action<BSONObject>)(response =>
                        {
                            try
                            {
                                Vector3i position = context.BlockPosition.Value;
                                Vector3i wrappedPosition = World.GetWrappedWorldPosition(position.XZ).X_Z(position.y);
                                Quaternion rotation = Quaternion.Identity;

                                if ((current as WorldObjectItem).TryPlaceObject(context.Player, wrappedPosition, rotation))
                                {
                                    var obj = WorldObjectManager.TryPlaceWorldObject(context.Player, (context.SelectedItem as WorldObjectItem), wrappedPosition, rotation);
                                }
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine();
                                Testcommand.exceptionMessage.Add("Exception throw while placing object with item " + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Exception throw while placing object with item " + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                                Console.ResetColor();
                                user.Inventory.Toolbar.Clear();
                            }
                        }), (context.SelectedItem as WorldObjectItem).WorldObjectType.Name);
                        Thread.Sleep(100);
                        Console.WriteLine();
                        foreach (WorldObject obj in WorldObjectManager.All)
                            obj.Destroy();
                        if (user.Client.Connected == false)
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine("User no more connected - Stopping actual test.");
                            Console.ResetColor();
                            return;
                        }
                    }
                }
                catch (Exception e)
                {
                    Testcommand.exceptions.Add(e);
                    Console.WriteLine();
                    Testcommand.exceptionMessage.Add("Exception throw while placing object with item " + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Exception throw while placing object with item " + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                    Console.ResetColor();
                }
            }
            stopWatch.Stop();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("All item given in " + stopWatch.ElapsedMilliseconds + "ms\n");
            Console.ResetColor();
        }
    }
}
