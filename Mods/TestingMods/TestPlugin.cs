﻿using Asphalt;
using Asphalt.Api.Event;
using Asphalt.Api.Event.PlayerEvents;
using Eco.Core.Plugins.Interfaces;
using Eco.Core.Utils;
using Eco.Gameplay.Animals;
using Eco.Gameplay.Items;
using Eco.Gameplay.Players;
using Eco.Shared.Items;
using Eco.Shared.Math;
using Eco.Shared.Utils;
using Eco.Simulation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Eco.Mods.TechTree
{
    [AsphaltPlugin]
    public class TestPlugin : IModKitPlugin, IInitializablePlugin
    {
        public TestPlugin() { }

        public override string ToString()
        {
            return "TestPlugin";
        }

        public string GetStatus()
        {
            return "TestPlugin";
        }

        public void Initialize(TimedTask timer)
        {
            TestListener listener = new TestListener();
            EventManager.RegisterListener(listener);
        }
    }

    public class TestListener
    {
        public TestListener()
        { }

        Dictionary<AnimalEntity, double> bisonMilk = new Dictionary<AnimalEntity, double>();

        int MILK_PRODUCE_TIME = 900;

        [EventHandler]
        public void interact(PlayerInteractEvent evt)
        {
            if (evt.Context.Method == InteractionMethod.Right && evt.Context.SelectedItem is JDBucketEmptyItem)
            {
                foreach (AnimalEntity animal in EcoSim.AnimalSim.All.Where(x => (x as AnimalEntity).Species.Name == "Bison"))
                {
                    float distance = Vector3.Distance(animal.Position, evt.Context.Player.Position);
                    if (distance < 3)
                    {
                        if (bisonMilk.ContainsKey(animal))
                        {
                            if (bisonMilk[animal] + MILK_PRODUCE_TIME <= TimeUtil.Seconds)
                            {
                                if (GetMilk(evt.Context.Player.User))
                                    bisonMilk[animal] = TimeUtil.Seconds;
                            } else
                            {
                                evt.Context.Player.SendTemporaryMessage($"Milk can only be collected again in {(int)(bisonMilk[animal] + MILK_PRODUCE_TIME - TimeUtil.Seconds)} seconds");
                            }
                        }
                        else
                        {
                            if (GetMilk(evt.Context.Player.User))
                                bisonMilk[animal] = TimeUtil.Seconds;
                        }
                        break;
                    }

                }
            }
        }

        private Result GetMilk(User user)
        {
            return user.Inventory.TryModify(changeSet =>
            {
                changeSet.RemoveItems<JDBucketEmptyItem>(1);
                changeSet.AddItems<JDMilkBucketItem>(1);
            }, user);
        }
    }
}
