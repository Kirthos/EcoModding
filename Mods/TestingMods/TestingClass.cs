﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kirthos.Mods
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using Eco.Gameplay.Blocks;
    using Eco.Gameplay.Components;
    using Eco.Gameplay.DynamicValues;
    using Eco.Gameplay.Interactions;
    using Eco.Gameplay.Items;
    using Eco.Gameplay.Players;
    using Eco.Gameplay.Skills;
    using Eco.Gameplay.Systems.TextLinks;
    using Eco.Shared.Serialization;
    using Eco.Shared.Utils;
    using Eco.World;
    using Eco.World.Blocks;
    using Eco.Gameplay.Economy;
    using Eco.Gameplay.Pipes;
    using Eco.Gameplay.Objects;
    using Eco.Shared.Math;
    using Eco.Shared.Networking;
    using Eco.Gameplay.Systems.Chat;
    using Eco.Gameplay.Pipes.LiquidComponents;
    using Eco.Mods.TechTree;
    using System.Threading;
    using Eco.Simulation.WorldLayers.Layers;
    using Eco.Gameplay.Wires;
    using Eco.Shared.Items;
    using Eco.Shared.Localization;
    using System.Reflection;
    using Eco.Core.Utils;
    using System.Runtime.Remoting.Contexts;
    using System.Diagnostics;
    using Kirthos.Mods.Testing;
    using System.IO;


    /*
     * Copyright (c) 2018 [Kirthos]
     * 
     * Created by Kirthos 05/26/2018
     */

    public class Testcommand : IChatCommandHandler
    {
        public static List<string> exceptionMessage = new List<string>();
        public static List<Exception> exceptions = new List<Exception>();

        [ChatCommand("A testing command that spawn in stockpile every carried item, in chest every other item, place all block registered and all worldObject")]
        public static void test(User user, bool testItem, bool testBlock, bool testObject, bool isForConsole)
        {
            new Thread(() =>
            {
                Vector3i userPos = user.Position.Ceiling;
                int i = 0;
                int j = 0;
                int k = 0;
                int l = 0;
                int m = 0;
                Stopwatch stopWatch = new Stopwatch();
                Stopwatch GstopWatch = new Stopwatch();
                GstopWatch.Start();
                if (testItem)
                {
                    StockpileObject stockpile = WorldObjectManager.TryToAdd<StockpileObject>(user, userPos + Vector3i.Forward * 3 + Vector3i.Up * 1, Quaternion.Identity);
                    StorageChestObject chest = WorldObjectManager.TryToAdd<StorageChestObject>(user, userPos + Vector3i.Forward * 1 + Vector3i.Up * 1 + Vector3i.Right * 3, Quaternion.Identity);
                    foreach (Item it in Item.AllItems)
                    {
                        if (it.IsCarried)
                        {
                            stockpile.GetComponent<PublicStorageComponent>().Inventory.TryAddItems(it.GetType(), 5);
                            i++;
                        }
                        else if (it.GetType().HasAttribute(typeof(SerializedAttribute)))
                        {
                            chest.GetComponent<PublicStorageComponent>().Inventory.TryAddItems(it.GetType(), it.MaxStackSize);
                            k++;
                        }
                        if (i >= 25)
                        {
                            i = 0;
                            j++;
                            stockpile = WorldObjectManager.TryToAdd<StockpileObject>(user, userPos + Vector3i.Forward * 3 + Vector3i.Up * (1 + j * 5), Quaternion.Identity);
                        }
                        if (k >= 16)
                        {
                            k = 0;
                            m++;
                            if (m >= 5)
                            {
                                m = 0;
                                l++;
                            }
                            chest = WorldObjectManager.TryToAdd<StorageChestObject>(user, userPos + Vector3i.Forward * (1 + m) + Vector3i.Up * (1 + l) + Vector3i.Right * 3, Quaternion.Identity);
                        }
                    }
                }
                if (testObject)
                {
                    i = 0;
                    j = 0;
                    k = 0;
                    foreach (Type t in WorldObjectManager.Obj.AllWorldObjectTypes.Values)
                    {
                        try
                        {
                            if (isForConsole == false)
                            {
                                SmallHangingHewnLogSignObject sign = WorldObjectManager.TryToAdd<SmallHangingHewnLogSignObject>(user, new Vector3i(i * 8, j * 8, k * 8) + userPos + Vector3i.Back * 3 + Vector3i.Right * 20, Quaternion.ToQuaternion(new Vector3(0, 180, 0))); if (t.HasAttribute(typeof(SerializedAttribute)))
                                    if (sign != null)
                                        sign.GetComponent<CustomTextComponent>().SetText(user.Player, t.ToString().Split('.').Last());
                                for (l = -3; l <= 3; l++)
                                {
                                    for (m = -3; m <= 3; m++)
                                    {
                                        World.SetBlock(typeof(FlatSteelFloorBlock), new Vector3i(i * 8, j * 8, k * 8) + userPos + Vector3i.Right * 20 + Vector3i.Forward + new Vector3i(l, -1, m));
                                    }
                                }
                            }
                            WorldObject obj = WorldObjectManager.TryToAdd(t, user, new Vector3i(i * 8, j * 8, k * 8) + userPos + Vector3i.Right * 20, Quaternion.Identity);
                            if (obj != null)
                            {
                                if (isForConsole)
                                    obj.Destroy();
                            }
                        }

                        catch (Exception e)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Exception throw while trying to place " + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                            Console.ResetColor();
                        }
                        if (isForConsole == false)
                        {
                            i++;
                            if (i >= 5)
                            {
                                i = 0;
                                k++;
                                if (k >= 5)
                                {
                                    j++;
                                    k = 0;
                                }
                            }
                        }
                    }
                }
                GstopWatch.Stop();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Finish in " + GstopWatch.ElapsedMilliseconds + "ms");
                Console.ResetColor();
            }).Start();
        }

        [ChatCommand("Test park")]
        public static void testpark(User user)
        {
            test(user, true, true, true, false);
        }

        [ChatCommand("Test all")]
        public static void testall(User user, bool testMore = false)
        {
            Stopwatch all = new Stopwatch();
            all.Start();
            exceptionMessage.Clear();
            user.Player.RPC("ToggleFly");
            new Thread(() =>
            {
                TestBlock.TestPlaceAllBlock(user);
                TestBlock.TestPickupBlock(user);
                if (testMore)
                {
                    TestBlock.TestDigBlock(user);
                    TestBlock.TestHammerBlock(user);
                    TestBlock.TestMineBlock(user);
                }
                TestObject.TestPlaceObject(user);
                if (testMore)
                    TestObject.TestPickupObject(user);
                TestItem.TestGiveAllItem(user);
                TestItem.TestInteractAir(user);
                TestRecipe.GiveAllSkill(user);
                TestRecipe.TestAllRecipe(user);
                TestObject.TestInteractObject(user);
                all.Stop();
                Console.WriteLine("Finished testing with " + exceptionMessage.Count + " exception. Taking " + all.ElapsedMilliseconds/1000 + "," + all.ElapsedMilliseconds%1000 + "ms");
                Console.ForegroundColor = ConsoleColor.Red;
                foreach (string msg in exceptionMessage)
                {
                    if (msg.Contains("MissingMethodException"))
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine(msg);
                    }
                }
                foreach (string msg in exceptionMessage)
                {
                    if (msg.Contains("MissingMethodException") == false)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(msg);
                    }
                }
                Console.ResetColor();
                LogExceptionInfiles();
            }).Start();
        }

        [ChatCommand("Test object")]
        public static void testobject(User user, bool testMore = false)
        {
            exceptionMessage.Clear();
            user.Player.RPC("ToggleFly");
            new Thread(() =>
            {
                TestObject.TestPlaceObject(user);
                if (testMore)
                    TestObject.TestPickupObject(user);
                TestObject.TestInteractObject(user);
                Console.WriteLine("Finished testing objects with " + exceptionMessage.Count + " exception.");
                Console.ForegroundColor = ConsoleColor.Red;
                foreach (string msg in exceptionMessage)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    if (msg.Contains("MissingMethodException"))
                        Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(msg);
                }
                Console.ResetColor();
            }).Start();
        }

        [ChatCommand("Test block")]
        public static void testblock(User user, bool testMore = false)
        {
            exceptionMessage.Clear();
            new Thread(() =>
            {
                TestBlock.TestPlaceAllBlock(user);
                TestBlock.TestPickupBlock(user);
                if (testMore)
                {
                    TestBlock.TestDigBlock(user);
                    TestBlock.TestHammerBlock(user);
                    TestBlock.TestMineBlock(user);
                }
                Console.WriteLine("Finished testing blocks with " + exceptionMessage.Count + " exception.");
                foreach (string msg in exceptionMessage)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    if (msg.Contains("MissingMethodException"))
                        Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(msg);
                }
                Console.ResetColor();
            }).Start();
        }

        [ChatCommand("Test item")]
        public static void testitem(User user)
        {
            exceptionMessage.Clear();
            user.Player.RPC("ToggleFly");
            new Thread(() =>
            {
                TestItem.TestGiveAllItem(user);
                TestItem.TestInteractAir(user);
                //TestItem.TestPlaceWorldObjectItem(user);
                Console.WriteLine("Finished testing item with " + exceptionMessage.Count + " exception.");
                foreach (string msg in exceptionMessage)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    if (msg.Contains("MissingMethodException"))
                        Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(msg);
                }
                Console.ResetColor();
            }).Start();
        }

        [ChatCommand("Test recipe")]
        public static void testrecipe(User user)
        {
            exceptionMessage.Clear();
            new Thread(() =>
            {
                TestRecipe.GiveAllSkill(user);
                TestRecipe.TestAllRecipe(user);
                Console.WriteLine("Finished testing recipe with " + exceptionMessage.Count + " exception.");
                foreach (string msg in exceptionMessage)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    if (msg.Contains("MissingMethodException"))
                        Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(msg);
                }
                Console.ResetColor();
                LogExceptionInfiles();
            }).Start();
        }

        private static void LogExceptionInfiles()
        {
            string Normal = null;
            string Full = null;
            int id = 0;
            string[] message = exceptionMessage.ToArray();
            foreach (Exception e in exceptions)
            {
                Normal += "=====EXCEPTION " + id + "=====";
                Full += "=====EXCEPTION " + id + "=====";
                if (message[id].Contains("MissingMethodException") == false)
                    Normal += message[id] + "\n" + e.ToStringPretty() + "\n\n";
                Full += message[id] + "\n" + e.ToStringPretty() + "\n\n";
                id++;
            }
            if (Normal != null)
                File.WriteAllText("./Dump/AutomatedTestReport.log", Normal);
            if (Full != null)
                File.WriteAllText("./Dump/AutomatedFULLTestReport.log", Full);
        }

        [ChatCommand("A testing tool")]
        public static void testtool(User user)
        {
            user.Inventory.TryAddItem<ToddToolItem>();
        }
    }
    [Serialized]
    [Weight(0)]
    [Category("Tool")]
    public class ToddToolItem : ToolItem
    {
        public override string Description { get { return "Now...It Just Works"; } }
        public override string FriendlyName { get { return "Todd Tool"; } }

        public override ClientPredictedBlockAction LeftAction { get { return ClientPredictedBlockAction.None; } }
        public override string LeftActionDescription { get { return "Work !"; } }

        private static SkillModifiedValue skilledRepairCost = new SkillModifiedValue(0, IndustrialEngineeringEfficiencySkill.MultiplicativeStrategy, typeof(IndustrialEngineeringSkill), Localizer.DoStr("repair cost"));
        public override IDynamicValue SkilledRepairCost { get { return skilledRepairCost; } }


        public override InteractResult OnActLeft(InteractionContext context)
        {
            if (context.HasTarget)
            {
                if (context.Target is WorldObject)
                {
                    WorldObject obj = context.Target as WorldObject;
                    if (obj.GetComponent<FuelSupplyComponent>() != null)
                    {
                        Item item = Item.Create(obj.GetComponent<FuelSupplyComponent>().FuelTypes.First());
                        obj.GetComponent<FuelSupplyComponent>().Inventory.TryAddItems(item.GetType(), item.MaxStackSize);
                    }
                    if (obj.GetComponent<CraftingComponent>() != null)
                    {
                        CraftingComponent comp = obj.GetComponent<CraftingComponent>();
                        if (comp.WorkOrders.Count > 0)
                        {
                            ThreadSafeList<ItemStack> tList = new ThreadSafeList<ItemStack>();
                            foreach (ItemStack i in comp.WorkOrders.FirstOrDefault().MissingIngredients)
                                tList.Add(i);
                            comp.WorkOrders.FirstOrDefault().MissingIngredients.Clear();
                            typeof(WorkOrder).GetFields(BindingFlags.NonPublic | BindingFlags.Instance).Where(x => x.Name.Contains("addedIngredients")).First().SetValue(comp.WorkOrders.FirstOrDefault(), tList);
                        }
                    }
                }
            }
            return InteractResult.Success;
        }

        public override InteractResult OnActInteract(InteractionContext context)
        {
            return InteractResult.NoOp;
        }

        public override bool ShouldHighlight(Type block)
        {
            return true;
        }
    }
}