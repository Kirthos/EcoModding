﻿using Eco.Gameplay.Interactions;
using Eco.Gameplay.Items;
using Eco.Gameplay.Objects;
using Eco.Gameplay.Pipes.Gases;
using Eco.Gameplay.Players;
using Eco.Mods.TechTree;
using Eco.Shared.Items;
using Eco.Shared.Math;
using Eco.World;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kirthos.Mods.Testing
{
    class TestObject
    {
        public static void TestPlaceObject(User user)
        {
            Stopwatch stopWatch = new Stopwatch();
            Vector3i userPos = user.Position.Ceiling + Vector3i.Up * 50;
            stopWatch.Restart();
            bool work = false;
            foreach (Type t in WorldObjectManager.Obj.AllWorldObjectTypes.Values)
            {
                WorldObject obj1 = null;
                WorldObject obj2 = null;
                try
                {
                //  if (t == typeof(HandPloughObject)) work = true;
                //  if (work == false) continue;
                    obj1 = WorldObjectManager.TryToAdd(t, user, userPos + Vector3i.Forward * 2, Quaternion.Identity, false);
                    if (obj1 != null)
                    {
                        Console.WriteLine("Place object " + t.ToString().Split('.').Last());
                        user.Player.SetPosition(userPos);
                        Thread.Sleep(5);
                        if (user.Client.Connected == false)
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine("User no more connected - Stopping actual test.");
                            Console.ResetColor();
                            return;
                        }
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine("Can't place object " + t.ToString().Split('.').Last());
                        Console.ResetColor();
                    }
                    obj2 = WorldObjectManager.TryToAdd(t, user, userPos + Vector3i.Forward * 2, Quaternion.Identity, false);
                    if (obj2 != null)
                    {
                        Console.WriteLine("Place object " + t.ToString().Split('.').Last());
                        user.Player.SetPosition(userPos);
                        Thread.Sleep(5);
                        if (user.Client.Connected == false)
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine("User no more connected - Stopping actual test.");
                            Console.ResetColor();
                            return;
                        }
                    }
                    else
                    {
                        //return;
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine("Can't place object " + t.ToString().Split('.').Last());
                        Console.ResetColor();
                    }
                }
                catch (Exception e)
                {
                    Testcommand.exceptions.Add(e);
                    //if (e is MissingMethodException == false)
                    Testcommand.exceptionMessage.Add("Exception throw while trying to place " + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Exception throw while trying to place " + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                    Console.ResetColor();
                }
                try
                {
                    Thread.Sleep(10);
                    if (obj1 != null) obj1.Destroy();
                    if (obj2 != null) obj2.Destroy();
                }
                catch(Exception)
                { }
            }
            stopWatch.Stop();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("All object placed in " + stopWatch.ElapsedMilliseconds + "ms\n");
            Console.ResetColor();
        }

        public static void TestInteractObject(User user)
        {
            Stopwatch stopWatch = new Stopwatch();
            Vector3i userPos = user.Position.Ceiling + Vector3i.Up * 50;
            stopWatch.Restart();
            bool work = false;
            foreach (Type t in WorldObjectManager.Obj.AllWorldObjectTypes.Values)
            {
                try
                {
                    if (t == typeof(HandPloughObject)) work = true;
                    if (t.BaseType == typeof(PhysicsWorldObject)) continue; // Don't try physics items
                    if (work == false) continue;
                    WorldObject obj = WorldObjectManager.TryToAdd(t, user, userPos + Vector3i.Forward * 2, Quaternion.Identity);
                    if (obj != null)
                    {
                        Console.Write("Interract with object " + t.ToString().Split('.').Last());
                        user.Player.SetPosition(userPos);
                        InteractionInfo info = new InteractionInfo();
                        info.TargetObject = obj;
                        info.Position = obj.Position;
                        info.Distance = Vector3.Distance(user.Player.Position, obj.Position);
                        InteractionContext context = null;
                        info.Method = InteractionMethod.Left;
                        context = info.MakeContext(user.Player);
                        Console.Write(" - Left");
                        obj.OnActLeft(context);
                        info.Method = InteractionMethod.Right;
                        context = info.MakeContext(user.Player);
                        Console.Write(" - Right");
                        obj.OnActRight(context);
                        info.Method = InteractionMethod.Interact;
                        context = info.MakeContext(user.Player);
                        Console.Write(" - interact");
                        obj.OnActInteract(context);
                        Thread.Sleep(300);
                        obj.Destroy();
                        Console.WriteLine();
                        Thread.Sleep(50);
                        if (user.Client.Connected == false)
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine("User no more connected - Stopping actual test.");
                            Console.ResetColor();
                            return;
                        }
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine("Can't place object " + t.ToString().Split('.').Last());
                        Console.ResetColor();
                    }
                }

                catch (Exception e)
                {
                    Testcommand.exceptions.Add(e);
                    Console.WriteLine();
                    //if (e is MissingMethodException == false)
                        Testcommand.exceptionMessage.Add("Exception throw while trying to interact with" + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Exception throw while trying to interact with " + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                    Console.ResetColor();
                }
            }
            stopWatch.Stop();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("All object interaction in " + stopWatch.ElapsedMilliseconds + "ms\n");
            Console.ResetColor();
        }

        public static void TestPickupObject(User user)
        {
            Stopwatch stopWatch = new Stopwatch();
            Vector3i userPos = user.Position.Ceiling + Vector3i.Up * 50;
            stopWatch.Restart();
            user.Inventory.Toolbar.Clear();
            user.Inventory.SetWeightLimit(999999);
            List<HammerItem> tools = new List<HammerItem>();
            foreach (Item it in Item.AllItems)
            {
                if (it is HammerItem)
                {
                    tools.Add(it as HammerItem);
                    user.Inventory.Toolbar.TryAddItem(it);
                }
            }
            bool work = false;
            foreach (Type t in WorldObjectManager.Obj.AllWorldObjectTypes.Values)
            {
                try
                {
                    if (t == typeof(HandPloughObject)) work = true;
                    if (t.BaseType == typeof(PhysicsWorldObject)) continue; // Don't try physics items
                    if (work == false) continue;
                    if (WorldObjectItem.GetCreatingItemFromType(t) == null)
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine("No Item exist for " + t.ToString().Split('.').Last());
                        Console.ResetColor();
                        continue;
                    }
                    foreach (HammerItem p in tools)
                    {
                        WorldObject obj = WorldObjectManager.TryToAdd(t, user, userPos + Vector3i.Forward * 2, Quaternion.Identity);
                        if (obj != null)
                        {
                            Console.WriteLine("Pickup object " + t.ToString().Split('.').Last() + " with " + p.FriendlyName);
                            user.Player.SetPosition(userPos);
                            InteractionInfo info = new InteractionInfo();
                            info.TargetObject = obj;
                            info.Position = obj.Position;
                            info.Distance = Vector3.Distance(user.Player.Position, obj.Position);
                            InteractionContext context = null;
                            info.Method = InteractionMethod.Right;
                            context = info.MakeContext(user.Player);
                            p.OnActRight(context);
                            info.Method = InteractionMethod.Left;
                            context = info.MakeContext(user.Player);
                            p.OnActLeft(context);
                            Thread.Sleep(50);
                            user.Inventory.Backpack.Clear();
                            if (user.Client.Connected == false)
                            {
                                Console.ForegroundColor = ConsoleColor.Cyan;
                                Console.WriteLine("User no more connected - Stopping actual test.");
                                Console.ResetColor();
                                return;
                            }
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("Can't place object " + t.ToString().Split('.').Last());
                            Console.ResetColor();
                        }
                    }
                }
                catch (Exception e)
                {
                    Testcommand.exceptions.Add(e);
                    Console.WriteLine();
                    //if (e is MissingMethodException == false)
                        Testcommand.exceptionMessage.Add("Exception throw while trying to pickup" + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Exception throw while trying to pickup " + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                    Console.ResetColor();
                }
            }
            stopWatch.Stop();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("All object pickup in " + stopWatch.ElapsedMilliseconds + "ms\n");
            Console.ResetColor();
        }
    }
}
