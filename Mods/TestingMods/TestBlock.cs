﻿using Eco.Gameplay.Components;
using Eco.Gameplay.Interactions;
using Eco.Gameplay.Items;
using Eco.Gameplay.Objects;
using Eco.Gameplay.Players;
using Eco.Mods.TechTree;
using Eco.Shared.Items;
using Eco.Shared.Math;
using Eco.Shared.Serialization;
using Eco.Shared.Utils;
using Eco.World;
using Eco.World.Blocks;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kirthos.Mods.Testing
{
    class TestBlock
    {

        public static void Test(bool isForConsole, User user)
        {
            Stopwatch stopWatch = new Stopwatch();
            int i = 0;
            int j = 0;
            Vector3i userPos = user.Position.Ceiling;
            Item tool = Item.Create<ModernPickaxeItem>();
            user.Inventory.AddItem(tool);
            foreach (Type t in BlockManager.BlockTypes)
            {
                try
                {
                    if (isForConsole == false)
                        WorldObjectManager.TryToAdd<SmallHangingHewnLogSignObject>(user, new Vector3i(i, 0, j) + userPos + Vector3i.Right * 5 + Vector3i.Up, Quaternion.ToQuaternion(new Vector3(0, 180, 0))).GetComponent<CustomTextComponent>().SetText(user.Player, t.ToString().Split('.').Last());
                    if (t.HasAttribute(typeof(SerializedAttribute)))
                    {
                        stopWatch.Restart();
                        Vector3i blockPos = new Vector3i(i, 0, j) + userPos + Vector3i.Right * 5;
                        World.SetBlock(t, new Vector3i(i, 0, j) + userPos + Vector3i.Right * 5);
                        Block block = World.GetBlock(blockPos);
                        stopWatch.Restart();
                        Console.WriteLine("Interacting with " + t.ToString().Split('.').Last() + " ... ");
                        user.Player.SetPosition(new Vector3i(i, 0, j) + userPos + Vector3i.Right * 5 + Vector3i.Back * 2);
                        InteractionInfo info = new InteractionInfo();
                        info.BlockPosition = blockPos;
                        InteractionContext context = null;
                        info.Method = InteractionMethod.Left;
                        context = info.MakeContext(user.Player);
                        context.SelectedItem.OnActLeft(context);
                        info.Method = InteractionMethod.Right;
                        context = info.MakeContext(user.Player);
                        context.SelectedItem.OnActRight(context);
                        info.Method = InteractionMethod.Interact;
                        context = info.MakeContext(user.Player);
                        context.SelectedItem.OnActInteract(context);
                        stopWatch.Stop();
                        Console.Write("done in " + stopWatch.ElapsedMilliseconds + "ms\n");
                        Thread.Sleep(100);
                    }
                }
                catch (Exception e)
                {
                    Testcommand.exceptions.Add(e);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Exception throw while trying to place " + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                    Console.ResetColor();
                }
                if (isForConsole == false)
                {
                    i++;
                    if (i >= 10)
                    {
                        i = 0;
                        j++;
                    }
                }
            }
        }

        public static void TestPlaceAllBlock(User user)
        {
            Stopwatch stopWatch = new Stopwatch();
            Vector3i userPos = user.Position.Ceiling;
            stopWatch.Restart();
            foreach (Type t in BlockManager.BlockTypes)
            {
                try
                {
                    if (t.HasAttribute(typeof(SerializedAttribute)))
                    {
                        Console.WriteLine("Placing block " + t.ToString().Split('.').Last());
                        Vector3i blockPos = userPos + Vector3i.Forward * 2;
                        World.SetBlock(t, blockPos);
                        user.Player.SetPosition(userPos);
                        Thread.Sleep(10);
                        if (user.Client.Connected == false)
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine("User no more connected - Stopping actual test.");
                            Console.ResetColor();
                            return;
                        }
                    }
                }
                catch (Exception e)
                {
                    Testcommand.exceptions.Add(e);
                    //if (e is MissingMethodException == false)
                    Testcommand.exceptionMessage.Add("Exception throw while trying to place " + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Exception throw while trying to place " + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                    Console.ResetColor();
                }
            }
            stopWatch.Stop();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("All block placed in " + stopWatch.ElapsedMilliseconds + "ms\n");
            Console.ResetColor();
        }

        public static void TestPickupBlock(User user)
        {
            Stopwatch stopWatch = new Stopwatch();
            Vector3i userPos = user.Position.Ceiling;
            stopWatch.Restart();
            foreach (Type t in BlockManager.BlockTypes)
            {
                try
                {
                    if (t.BaseType == typeof(PickupableBlock))
                    {
                        Console.WriteLine("Placing pickup block " + t.ToString().Split('.').Last());
                        Vector3i blockPos = userPos + Vector3i.Forward * 2;
                        World.SetBlock(t, blockPos);
                        PickupableBlock block = World.GetBlock(blockPos) as PickupableBlock;
                        user.Player.SetPosition(userPos);
                        InteractionInfo info = new InteractionInfo();
                        info.BlockPosition = blockPos;
                        InteractionContext context = null;
                        info.Method = InteractionMethod.Interact;
                        context = info.MakeContext(user.Player);
                        block.OnActInteract(context);
                        user.Inventory.Toolbar.Clear();
                        user.Inventory.Carried.Clear();
                        Thread.Sleep(10);
                        if (user.Client.Connected == false)
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine("User no more connected - Stopping actual test.");
                            Console.ResetColor();
                            return;
                        }
                    }
                }
                catch (Exception e)
                {
                    Testcommand.exceptions.Add(e);
                    //if (e is MissingMethodException == false)
                    Testcommand.exceptionMessage.Add("Exception throw while trying to pick " + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Exception throw while trying to pick " + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                    Console.ResetColor();
                }
            }
            stopWatch.Stop();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("All block pickup in " + stopWatch.ElapsedMilliseconds + "ms\n");
            Console.ResetColor();
        }

        public static void TestMineBlock(User user)
        {
            Stopwatch stopWatch = new Stopwatch();
            Vector3i userPos = user.Position.Ceiling;
            stopWatch.Restart();
            user.Inventory.Toolbar.Clear();
            List<PickaxeItem> pickaxes = new List<PickaxeItem>();
            foreach(Item it in Item.AllItems)
            {
                if (it is PickaxeItem)
                {
                    pickaxes.Add(it as PickaxeItem);
                    user.Inventory.Toolbar.TryAddItem(it);
                }
            }

            foreach (Type t in BlockManager.BlockTypes)
            {
                try
                {
                    if (Block.Is<Minable>(t))
                    {
                        int slot = 0;
                        foreach (PickaxeItem p in pickaxes)
                        {
                            Console.WriteLine("Mine block " + t.ToString().Split('.').Last() + " with " + p.FriendlyName);
                            Vector3i blockPos = userPos + Vector3i.Forward * 2;
                            World.SetBlock(t, blockPos);
                            Block block = World.GetBlock(blockPos);
                            user.Player.SetPosition(userPos);
                            typeof(ToolbarInventory).GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Where(x => x.Name.Contains("SelectIndex")).First().Invoke(user.Inventory.Toolbar, new object[] { slot, user.Player });
                            slot++;
                            InteractionInfo info = new InteractionInfo();
                            info.BlockPosition = blockPos;
                            InteractionContext context = null;
                            info.Method = InteractionMethod.Right;
                            context = info.MakeContext(user.Player);
                            p.OnActRight(context);
                            info.Method = InteractionMethod.Left;
                            context = info.MakeContext(user.Player);
                            p.OnActLeft(context);
                            Thread.Sleep(10);
                            if (user.Client.Connected == false)
                            {
                                Console.ForegroundColor = ConsoleColor.Cyan;
                                Console.WriteLine("User no more connected - Stopping actual test.");
                                Console.ResetColor();
                                return;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Testcommand.exceptions.Add(e);
                    //if (e is MissingMethodException == false)
                    Testcommand.exceptionMessage.Add("Exception throw while trying to mine " + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Exception throw while trying to mine " + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                    Console.ResetColor();
                }
            }
            stopWatch.Stop();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("All block mined in " + stopWatch.ElapsedMilliseconds + "ms\n");
            Console.ResetColor();
        }

        public static void TestDigBlock(User user)
        {
            Stopwatch stopWatch = new Stopwatch();
            Vector3i userPos = user.Position.Ceiling;
            stopWatch.Restart();
            user.Inventory.Toolbar.Clear();
            List<ShovelItem> tools = new List<ShovelItem>();
            foreach (Item it in Item.AllItems)
            {
                if (it is ShovelItem)
                {
                    tools.Add(it as ShovelItem);
                    user.Inventory.Toolbar.TryAddItem(it);
                }
            }

            foreach (Type t in BlockManager.BlockTypes)
            {
                try
                {
                    if (Block.Is<Diggable>(t))
                    {
                        int slot = 0;
                        foreach (ShovelItem p in tools)
                        {
                            Console.WriteLine("Dig block " + t.ToString().Split('.').Last() + " with " + p.FriendlyName);
                            Vector3i blockPos = userPos + Vector3i.Forward * 2;
                            World.SetBlock(t, blockPos);
                            Block block = World.GetBlock(blockPos);
                            user.Player.SetPosition(userPos);
                            typeof(ToolbarInventory).GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Where(x => x.Name.Contains("SelectIndex")).First().Invoke(user.Inventory.Toolbar, new object[] { slot, user.Player });
                            slot++;
                            InteractionInfo info = new InteractionInfo();
                            info.BlockPosition = blockPos;
                            InteractionContext context = null;
                            info.Method = InteractionMethod.Right;
                            context = info.MakeContext(user.Player);
                            p.OnActRight(context);
                            info.Method = InteractionMethod.Left;
                            context = info.MakeContext(user.Player);
                            p.OnActLeft(context);
                            user.Inventory.Carried.Clear();
                            Thread.Sleep(10);
                            if (user.Client.Connected == false)
                            {
                                Console.ForegroundColor = ConsoleColor.Cyan;
                                Console.WriteLine("User no more connected - Stopping actual test.");
                                Console.ResetColor();
                                return;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Testcommand.exceptions.Add(e);
                    //if (e is MissingMethodException == false)
                    Testcommand.exceptionMessage.Add("Exception throw while trying to dig " + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Exception throw while trying to dig " + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                    Console.ResetColor();
                }
            }
            stopWatch.Stop();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("All block dig in " + stopWatch.ElapsedMilliseconds + "ms\n");
            Console.ResetColor();
        }

        public static void TestHammerBlock(User user)
        {
            Stopwatch stopWatch = new Stopwatch();
            Vector3i userPos = user.Position.Ceiling;
            stopWatch.Restart();
            user.Inventory.Toolbar.Clear();
            List<HammerItem> tools = new List<HammerItem>();
            foreach (Item it in Item.AllItems)
            {
                if (it is HammerItem)
                {
                    tools.Add(it as HammerItem);
                    user.Inventory.Toolbar.TryAddItem(it);
                }
            }

            foreach (Type t in BlockManager.BlockTypes)
            {
                try
                {
                    if (Block.Is<Constructed>(t))
                    {
                        int slot = 0;
                        foreach (HammerItem p in tools)
                        {
                            Console.WriteLine("Hammer block " + t.ToString().Split('.').Last() + " with " + p.FriendlyName);
                            Vector3i blockPos = userPos + Vector3i.Forward * 2;
                            World.SetBlock(t, blockPos);
                            Block block = World.GetBlock(blockPos);
                            user.Player.SetPosition(userPos);
                            typeof(ToolbarInventory).GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Where(x => x.Name.Contains("SelectIndex")).First().Invoke(user.Inventory.Toolbar, new object[] { slot, user.Player });
                            slot++;
                            InteractionInfo info = new InteractionInfo();
                            info.BlockPosition = blockPos;
                            InteractionContext context = null;
                            info.Method = InteractionMethod.Right;
                            context = info.MakeContext(user.Player);
                            p.OnActRight(context);
                            info.Method = InteractionMethod.Left;
                            context = info.MakeContext(user.Player);
                            p.OnActLeft(context);
                            user.Inventory.Carried.Clear();
                            Thread.Sleep(10);
                            if (user.Client.Connected == false)
                            {
                                Console.ForegroundColor = ConsoleColor.Cyan;
                                Console.WriteLine("User no more connected - Stopping actual test.");
                                Console.ResetColor();
                                return;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Testcommand.exceptions.Add(e);
                    //if (e is MissingMethodException == false)
                    Testcommand.exceptionMessage.Add("Exception throw while trying to hammer " + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Exception throw while trying to hammer " + t.ToString().Split('.').Last() + " - " + e.GetType() + ": " + e.Message);
                    Console.ResetColor();
                }
            }
            stopWatch.Stop();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("All block hammer in " + stopWatch.ElapsedMilliseconds + "ms\n");
            Console.ResetColor();
        }
    }
}
