﻿using Eco.Core.Utils;
using Eco.Gameplay.Components;
using Eco.Gameplay.Items;
using Eco.Gameplay.Objects;
using Eco.Gameplay.Players;
using Eco.Gameplay.Skills;
using Eco.Shared.Math;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kirthos.Mods.Testing
{
    class TestRecipe
    {
        public static void GiveAllSkill(User user)
        {
            foreach (SkillScroll scroll in Skill.AllItems.OfType<SkillScroll>())
                scroll.OnUsed(user.Player, new ItemStack(scroll, 1, user.Inventory));
            foreach (Skill s in user.Skillset.Skills)
                s.ForceSetLevel(s.MaxLevel);
            user.Inventory.Toolbar.Clear();
            user.Inventory.Backpack.Clear();
        }

        public static void TestAllRecipe(User user)
        {
            Stopwatch stopWatch = new Stopwatch();
            Vector3i userPos = user.Position.Ceiling;
            stopWatch.Restart();
            foreach (Type t in CraftingComponent.AllTableWorldObjects)
            {
                foreach (Recipe r in CraftingComponent.RecipesOnWorldObject(t))
                {
                    WorldObject obj = null;
                    try
                    {
                        obj = WorldObjectManager.TryToAdd(t, user, userPos + Vector3i.Forward * 2, Quaternion.Identity, false);
                    }
                    catch (Exception) { }
                    if (obj != null)
                    {
                        try
                        {
                            CraftingComponent comp = obj.GetComponent<CraftingComponent>();
                            Console.WriteLine("start recipe " + r.RecipeName + " on " + obj.FriendlyName);
                            comp.CreateWorkOrder(user.Player, r, 1);
                            if (comp.WorkOrders.Count > 0)
                            {
                                ThreadSafeList<ItemStack> tList = new ThreadSafeList<ItemStack>();
                                foreach (ItemStack i in comp.CurrentWorkOrder.MissingIngredients)
                                    tList.Add(i);
                                comp.CurrentWorkOrder.MissingIngredients.Clear();
                                typeof(WorkOrder).GetFields(BindingFlags.NonPublic | BindingFlags.Instance).Where(x => x.Name.Contains("addedIngredients")).First().SetValue(comp.CurrentWorkOrder, tList);
                                //Thread.Sleep(5);
                                comp.ProcessWorkOrders((float)comp.CurrentWorkOrder.TotalTime);
                                //Thread.Sleep(5);
                                //Console.Write(comp.WorkOrders);
                                try
                                {
                                    comp.CurrentWorkOrder.TryToCollectItems(user.Player);
                                }
                                catch (NullReferenceException e)
                                {
                                    Console.ForegroundColor = ConsoleColor.Yellow;
                                    Console.WriteLine("Can't collect item - Seems a random error");
                                    Console.ResetColor();
                                }
                                //Thread.Sleep(5);
                                user.Inventory.Toolbar.Clear();
                                user.Inventory.Carried.Clear();
                                if (user.Client.Connected == false)
                                {
                                    Console.ForegroundColor = ConsoleColor.Cyan;
                                    Console.WriteLine("User no more connected - Stopping actual test.");
                                    Console.ResetColor();
                                    return;
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine();
                            Testcommand.exceptions.Add(e);
                            Testcommand.exceptionMessage.Add("Exception throw when crafting " + r.RecipeName + " on " + obj.FriendlyName + " - " + e.GetType() + ": " + e.Message);
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Exception throw when crafting " + r.RecipeName + " on " + obj.FriendlyName + " - " + e.GetType() + ": " + e.Message);
                            Console.ResetColor();
                        }
                        obj.Destroy();
                    }
                }
            }
            stopWatch.Stop();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("All recipe tested in " + stopWatch.ElapsedMilliseconds + "ms\n");
            Console.ResetColor();
        }
    }
}
