﻿using Eco.Gameplay.Items;
using Eco.Gameplay.Systems.TextLinks;
using System;

namespace Eco.Mods.TechTree
{
    public class CustomRestriction : InventoryRestriction
    {
        private Type acceptedItem;

        private string message = "";
        public override string Message {
            get
            {
                return this.message;
            }
        }

        public CustomRestriction(Type acceptedItem)
        {
            this.acceptedItem = acceptedItem;
        }

        public override int MaxAccepted(Item item, int currentQuantity)
        {
            if (item.Type != acceptedItem)
            {
                message = "You can put only " + Item.Get(acceptedItem).UILink();
                return 0;
            }
            return item.MaxStackSize;
        }
    }
}
