﻿using Eco.Core.Utils;
using Eco.Core.Utils.AtomicAction;
using Eco.Gameplay.Auth;
using Eco.Gameplay.DynamicValues;
using Eco.Gameplay.Interactions;
using Eco.Gameplay.Items;
using Eco.Gameplay.Objects;
using Eco.Gameplay.Plants;
using Eco.Gameplay.Stats;
using Eco.Gameplay.Systems.TextLinks;
using Eco.Mods.TechTree;
using Eco.Shared.Items;
using Eco.Shared.Localization;
using Eco.Shared.Math;
using Eco.Shared.Serialization;
using Eco.Shared.Utils;
using Eco.Simulation;
using Eco.World;
using Eco.World.Blocks;
using System;

namespace KirthosMods.GreenLeaf.MultiTool
{
    [Serialized]
    [CanMakeBlockForm(new[] { "Wall", "Floor", "Roof", "Stairs", "Window", "Fence", "Aqueduct", "Cube", "Column", "Ladder" })]
    public class MultiToolItem : HammerItem
    {
        public override LocString DisplayDescription => Localizer.DoStr("A tool that do everything");
        public override LocString DisplayName => Localizer.DoStr("Multi tool");

        public override ClientPredictedBlockAction LeftAction { get { return ClientPredictedBlockAction.None; } }
        public override LocString LeftActionDescription => Localizer.DoStr("Do");

        private static IDynamicValue AxeCalorieBurn = CreateCalorieValue(15, typeof(LoggingEfficiencySkill), typeof(MultiToolItem), new MultiToolItem().UILink());
        private static IDynamicValue HammerCalorieBurn = CreateCalorieValue(15, typeof(CalorieEfficiencySkill), typeof(MultiToolItem), new MultiToolItem().UILink());
        private static IDynamicValue PickaxeCalorieBurn = CreateCalorieValue(15, typeof(MiningEfficiencySkill), typeof(MultiToolItem), new MultiToolItem().UILink());
        private static IDynamicValue ScytheCalorieBurn = CreateCalorieValue(15, typeof(ScytheEfficiencySkill), typeof(MultiToolItem), new MultiToolItem().UILink());
        private static IDynamicValue ShovelCalorieBurn = CreateCalorieValue(15, typeof(ShovelEfficiencySkill), typeof(MultiToolItem), new MultiToolItem().UILink());

        private IDynamicValue calorieBurn = new ConstantValue(1f);
        public override IDynamicValue CaloriesBurn => calorieBurn;

        //private static SkillModifiedValue skilledRepairCost = new SkillModifiedValue(10, PipeCraftingSkill.MultiplicativeStrategy, typeof(PipeCraftingSkill), Localizer.DoStr("repair cost"));
        public override IDynamicValue SkilledRepairCost => new ConstantValue(10f);//{ get { return skilledRepairCost; } }

        public override float DurabilityRate { get { return DurabilityMax / 1000; } }

        public override Item RepairItem { get { return Item.Get<SteelItem>(); } }
        public override int FullRepairAmount { get { return 10; } }

        private static IDynamicValue damage = CreateDamageValue(2, typeof(LoggingDamageSkill), typeof(MultiToolItem), new MultiToolItem().UILink());
        public IDynamicValue Damage { get { return damage; } }

        public override InteractResult OnActLeft(InteractionContext context)
        {
            if (context.HasBlock)
            {
                var block = World.GetBlock(context.BlockPosition.Value);
                /// AXE
                if (block.Is<TreeDebris>())
                {
                    calorieBurn = AxeCalorieBurn;
                    InventoryChangeSet changes = new InventoryChangeSet(context.Player.User.Inventory, context.Player.User);
                    changes.AddItems<WoodPulpItem>(5);
                    IAtomicAction lawAction = PlayerActions.PickUp.CreateAtomicAction(context.Player, Get<WoodPulpItem>(), context.BlockPosition.Value);
                    return (InteractResult)this.PlayerDeleteBlock(context.BlockPosition.Value, context.Player, false, 3, null, changes, lawAction);
                }
                /// PICKAXE
                else if (block.Is<Minable>())
                {
                    calorieBurn = PickaxeCalorieBurn;

                    Result result;
                    if (block is IRepresentsItem)
                    {
                        Item item = Item.Get((IRepresentsItem)block);
                        IAtomicAction lawAction = PlayerActions.PickUp.CreateAtomicAction(context.Player, item, context.BlockPosition.Value);
                        result = this.PlayerDeleteBlock(context.BlockPosition.Value, context.Player, false, 1, null, lawAction);
                    }
                    else
                        result = this.PlayerDeleteBlock(context.BlockPosition.Value, context.Player, false, 1);

                    if (result.Success)
                        if (RubbleObject.TrySpawnFromBlock(context.Block.GetType(), context.BlockPosition.Value))
                            context.Player.User.UserUI.OnCreateRubble.Invoke();

                    return (InteractResult)result;
                }
                /// HAMMER
                else if (block.Is<Constructed>())
                {
                    calorieBurn = HammerCalorieBurn;
                    return (InteractResult)this.PlayerDeleteBlock(context.BlockPosition.Value, context.Player, true, 1);
                }
                /// HAMMER
                else if (block is WorldObjectBlock)
                {
                    calorieBurn = HammerCalorieBurn;
                    WorldObject obj = ((WorldObjectBlock)context.Block).WorldObjectHandle.Object;
                    Result authResult = AuthManager.IsAuthorized(obj, context.Player.User, AccessType.FullAccess);
                    if (!authResult.Success)
                        return (InteractResult)authResult;

                    Result pickupResult = obj.TryPickUp(context.Player);
                    if (pickupResult.Success)
                        this.BurnCalories(context.Player);
                    return (InteractResult)pickupResult;
                }
                /// SCYTHE
                else if (block.Is<Reapable>())
                {
                    calorieBurn = ScytheCalorieBurn;
                    var plant = EcoSim.PlantSim.GetPlant(context.BlockPosition.Value);
                    if (plant != null && plant is IHarvestable)
                    {
                        Result result = (plant as IHarvestable).TryHarvest(context.Player, true);
                        if (result.Success)
                        {
                            this.BurnCalories(context.Player);
                            context.Player.SpawnBlockEffect(context.BlockPosition.Value, context.Block.GetType(), BlockEffect.Harvest);
                        }

                        return (InteractResult)result;
                    }
                    else
                        return (InteractResult)this.PlayerDeleteBlock(context.BlockPosition.Value, context.Player, false);
                }
                /// SHOVEL
                else if (block is PlantBlock)
                {
                    calorieBurn = ShovelCalorieBurn;
                    var plant = EcoSim.PlantSim.GetPlant(context.BlockPosition.Value);
                    if (plant != null && plant is IHarvestable)
                    {
                        Result result = ((IHarvestable)plant).TryHarvest(context.Player, false);
                        if (result.Success)
                            this.BurnCalories(context.Player);
                        return (InteractResult)result;
                    }
                    else
                        return (InteractResult)this.PlayerDeleteBlock(context.BlockPosition.Value, context.Player, false);
                }
                /// SHOVEL
                else if (block.Is<Diggable>())
                {
                    calorieBurn = ShovelCalorieBurn;
                    if (TreeEntity.TreeRootsBlockDigging(context))
                        return InteractResult.Failure(Localizer.DoStr("You attempt to dig up the soil, but the roots are too strong!"));

                    IAtomicAction destroyAction = UsableItemUtils.TryDestroyPlant(context.Player, context.BlockPosition.Value + Vector3i.Up);
                    return (InteractResult)this.PlayerDeleteBlock(
                        context.BlockPosition.Value, context.Player, true, 1, new DirtItem(), destroyAction);
                }
            }
            /// AXE
            else if (context.Target is IDamageable)
            {
                calorieBurn = AxeCalorieBurn;
                if (((IDamageable)context.Target).TryApplyDamage(context.Player, this.Damage.GetCurrentValue(context.Player.User), context))
                    this.BurnCalories(context.Player);
                return InteractResult.Success;
            }
            /// PICKAXE
            else if (context.Target is RubbleObject)
            {
                calorieBurn = PickaxeCalorieBurn;
                var rubble = (RubbleObject)context.Target;
                if (rubble.IsBreakable)
                {
                    rubble.Breakup();
                    BurnCalories(context.Player);
                    return InteractResult.Success;
                }
            }
            /// HAMMER
            else if (context.Target is WorldObject)
            {
                calorieBurn = HammerCalorieBurn;
                WorldObject obj = context.Target as WorldObject;
                Result authResult = AuthManager.IsAuthorized(obj, context.Player.User, AccessType.FullAccess);
                if (!authResult.Success)
                    return (InteractResult)authResult;

                Result pickupResult = obj.TryPickUp(context.Player);
                if (pickupResult.Success)
                    this.BurnCalories(context.Player);
                return (InteractResult)pickupResult;
            }
            return InteractResult.NoOp;
        }
        public override InteractResult OnActRight(InteractionContext context)
        {
            return InteractResult.NoOp;
        }

        public override InteractResult OnActInteract(InteractionContext context)
        {
            return InteractResult.NoOp;
        }

        public override bool ShouldHighlight(Type block)
        {
            return true;
        }
    }
}
